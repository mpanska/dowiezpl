import axios from 'axios';
import accountService from './accountService';

const TRANSPORT_API_URL = "https://dowiez.pl:5001/api/Transports";
const headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};

class demandService{

    async getAllTransports(params){
        let headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};
        return await axios.get(`${TRANSPORT_API_URL}/search`,  {headers, params});
    }

    createTransport(data){
        return axios.post(TRANSPORT_API_URL, data, {headers});
    }

    async getUserTransports(){
        try {
            const {data: response} = await axios.get(`${TRANSPORT_API_URL}/my/`, {headers}); 
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    getTransport(transportId){
        return axios.get(`${TRANSPORT_API_URL}/${transportId}/`, {headers});
    }

    proposeTransport(transportId, demandId){
        return axios.post(`${TRANSPORT_API_URL}/${transportId}/propose/${demandId}`, {}, {headers: headers});
    }

    editTransport(body){
        return axios.put(TRANSPORT_API_URL, body, {headers});
    }

    async getDemandsForTransport(transportId){
        try {
            const {data: response} = await axios.get(`${TRANSPORT_API_URL}/${transportId}/demands`, {headers});
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    acceptDemand(transportId, demandId){
        return axios.post(`${TRANSPORT_API_URL}/${transportId}/accept/${demandId}`, {}, {headers: headers});
    }

    denyDemand(transportId, demandId){
        return axios.post(`${TRANSPORT_API_URL}/${transportId}/deny/${demandId}`, {}, {headers: headers});
    }

    cancelTransport(transportId){
        return axios.post(`${TRANSPORT_API_URL}/${transportId}/cancel`, {}, {headers});
    }

    startTransport(transportId){
        return axios.post(`${TRANSPORT_API_URL}/${transportId}/begin`, {}, {headers});
    }

    finishTransport(transportId){
        return axios.post(`${TRANSPORT_API_URL}/${transportId}/finish`, {}, {headers});
    }

}
export default new demandService()
