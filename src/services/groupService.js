import axios from 'axios';
import accountService from './accountService';

const GROUP_API_URL = "https://dowiez.pl:5001/api/Groups";
const headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};

class groupService{

    async getUserGroups(){
        try {
            const {data: response} = await axios.get(`${GROUP_API_URL}/my/`, {headers});
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    async getAllGroups(){
        try {
            const {data: response} = await axios.get(GROUP_API_URL, {headers});
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    createGroup(data){
        return axios.post(GROUP_API_URL, data, {headers});
    }

    joinGroup(groupId, params){
        return axios.post(`${GROUP_API_URL}/${groupId}/join`, {}, {headers, params});
    }

    leaveGroup(groupId){
        return axios.post(`${GROUP_API_URL}/${groupId}/leave`, {}, {headers});
    }

    editGroup(body){
        return axios.put(GROUP_API_URL, body, {headers});
    }

    deleteGroup(groupId){ 
        return axios.delete(`${GROUP_API_URL}/${groupId}`, {headers});
    }
}
export default new groupService()
