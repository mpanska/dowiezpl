import axios from 'axios';
import accountService from './accountService';

const REPORT_API_URL = "https://dowiez.pl:5001/api/Reports";
const headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};

class reportService{

    createReport(data){
        return axios.post(REPORT_API_URL, data, {headers});
    }

}
export default new reportService()
