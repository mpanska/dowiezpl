import axios from 'axios';
import accountService from './accountService';

const OPINION_API_URL = "https://dowiez.pl:5001/api/Opinions";
const headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};

class opinionService{

    async publishOpinion(data){
        return await axios.post(OPINION_API_URL, data, {headers});
    }

    async getOpinions(userId){
        try {
            const {data: response} = await axios.get(`${OPINION_API_URL}/about/${userId}`, {headers}); //use data destructuring to get data from the promise object
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    async getUserOpinions(){
        try {
            const {data: response} = await axios.get(`${OPINION_API_URL}/my`, {headers}); //use data destructuring to get data from the promise object
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    deleteOpinion(opinionId){
        return axios.delete(`${OPINION_API_URL}/${opinionId}`, {headers});
    }

    editOpinion(body){
        return axios.put(OPINION_API_URL, body, {headers});
    }

}
export default new opinionService()