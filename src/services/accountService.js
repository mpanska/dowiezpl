import axios from 'axios';
import Cookies from 'universal-cookie';

const ACCOUNT_API_URL = "https://dowiez.pl:5001/api/Accounts";
const cookie = new Cookies();
const headers = {'Authorization': `Bearer ${cookie.get("token")}`};

class accountService{
    getAuthToken() {
        if(cookie.get("token") === null || cookie.get("token") === undefined){
            document.body.style.backgroundColor = "#FFFCFA";
        }
        else{
            document.body.style.backgroundColor = "#FFECE1";
        }
        return cookie.get("token");
    }

    logIn(email, password) {
        return axios.post(ACCOUNT_API_URL + "/login", { email, password });
    }

    logOut() {
        cookie.remove("token");
        window.location.replace("/");
        window.location.reload();
    }

    createAccount(firstName, lastName, email, password) {
        return axios.post(ACCOUNT_API_URL + "/create", {email, password, firstName, lastName});
    }

    confirmEmail(userId, token){
        return axios.post(`${ACCOUNT_API_URL}/confirmEmail`, { userId, token });
    }

    async getCurrentUserData(){
        try {
            const {data: response} = await axios.get(`${ACCOUNT_API_URL}/my`, {headers});
            return Object(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    editUserData(body){
        return axios.put(ACCOUNT_API_URL, body, {headers});
    }

    requestResetPassword(params){
        return axios.post(ACCOUNT_API_URL + "/requestResetPassword", {},  {params});
    }

    resetPassword(body){
        return axios.put(ACCOUNT_API_URL + "/resetPassword", body);
    }

    changePassword(body){
        return axios.put(ACCOUNT_API_URL + "/changePassword", body, {headers});
    }

}
export default new accountService()