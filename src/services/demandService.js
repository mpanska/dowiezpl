import axios from 'axios';
import accountService from './accountService';

const DEMAND_API_URL = "https://dowiez.pl:5001/api/Demands";
const headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};

class demandService{

    async getAllDemands(params){
        let headers = {'Authorization': `Bearer ${accountService.getAuthToken()}`};
        return await axios.get(`${DEMAND_API_URL}/search`, {headers, params});
    }

    async getAllDemandsArray(params){
        try {
            const {data: response} = await axios.get(`${DEMAND_API_URL}/search/`, {headers, params}); //use data destructuring to get data from the promise object
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }
  
    async getDemand(demandId){
        try {
            const {data: response} = await axios.get(`${DEMAND_API_URL}/${demandId}`, {headers}); //use data destructuring to get data from the promise object
            return Object(response);
        }
        catch (error) {
            console.log(error);
        }
    }

    getUserDemand(userId){ 
        return axios.get(`${DEMAND_API_URL}/user/${userId}`, {headers});
    }

    async getUserDemands(){
        try {
            const {data: response} = await axios.get(`${DEMAND_API_URL}/my/`, {headers}); //use data destructuring to get data from the promise object
            return Array(response);
        } 
        catch (error) {
            console.log(error);
        }
    }

    createNewDemand(data){
        return axios.post(DEMAND_API_URL, data, {headers});
    }

    getCities(){
        return axios.get("https://dowiez.pl:5001/api/Cities", {headers});
    }

    reguestATransport(demandId, transportId){
        return axios.post(`${DEMAND_API_URL}/${demandId}/request/${transportId}`, {}, {headers});
    }

    editDemand(body){
        return axios.put(DEMAND_API_URL, body, {headers});
    }

    cancelDemand(demandId){
        return axios.post(`${DEMAND_API_URL}/${demandId}/cancel`, {}, {headers});
    }

    acceptTransport(demandId){
        return axios.post(`${DEMAND_API_URL}/${demandId}/acceptProposition`, {}, {headers});
    }

    denyTransportProposition(demandId){
        return axios.post(`${DEMAND_API_URL}/${demandId}/denyProposition`, {}, {headers});
    }

    cancelTransportforDemand(demandId){
        return axios.post(`${DEMAND_API_URL}/${demandId}/cancelTransport`, {}, {headers});
    }

}
export default new demandService()
