import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import './scss/index.css';
import Navbar from './components/elements/Navbar'
import Footer from './components/elements/Footer'
import Home from "./components/Home";
import Transports from "./components/transportComponents/Transports";
import Demands from "./components/demandComponents/Demands";
import UserAccount from "./components/UserAccount";
import CreateDemand from "./components/demandComponents/CreateDemand";
import CreateTransport from "./components/transportComponents/CreateTransport";
import DemandPage from "./components/demandComponents/DemandPage";
import UserDemands from "./components/demandComponents/UserDemands";
import { ProtectedRoute } from "./utils/ProtectedRoute";
import OtherUser from "./components/OtherUser";
import UserTransports from "./components/transportComponents/UserTransports";
import TransportPage from "./components/transportComponents/TransportPage";
import EditUserData from "./components/EditUserData";
import EditDemand from "./components/demandComponents/EditDemand";
import Groups from "./components/groupComponents/Groups";
import CreateGroup from "./components/groupComponents/CreateGroup";
import EditTransport from "./components/transportComponents/EditTransport";
import GroupsSearch from "./components/groupComponents/GroupsSearch";
import ReportPage from "./components/ReportPage";
import Opinion from "./components/Opinion";
import EditGroup from "./components/groupComponents/EditGroup";
import EditOpinion from "./components/EditOpinion";
import DemandsInGroup from "./components/demandComponents/DemandsInGroup";

function MainApp() {
  return (
    <Router>
      <Navbar/>
      <Switch>
          <Route exact path='/' component={Home} />
          <ProtectedRoute exact path='/transport' component={Transports} />
          <ProtectedRoute exact path='/demand'  component={Demands} />
          <ProtectedRoute exact path='/account' component={UserAccount} />
          <ProtectedRoute exact path="/createDemand" component={CreateDemand} />
          <ProtectedRoute exact path="/createTransport" component={CreateTransport} />
          <ProtectedRoute exact path='/demand/:demandId' component={DemandPage} />
          <ProtectedRoute exact path='/transport/:transportId' component={TransportPage} />
          <ProtectedRoute exact path='/myDemands' component={UserDemands} />
          <ProtectedRoute exact path='/myTransports' component={UserTransports} />
          <ProtectedRoute exact path='/user/:accountId' component={OtherUser} />
          <ProtectedRoute exact path='/editUserData' component={EditUserData} />
          <ProtectedRoute exact path='/editDemand' component={EditDemand} />
          <ProtectedRoute exact path='/editTransport' component={EditTransport} />
          <ProtectedRoute exact path='/myGroups' component={Groups} />
          <ProtectedRoute exact path='/createGroup' component={CreateGroup} />
          <ProtectedRoute exact path='/editGroup' component={EditGroup} />
          <ProtectedRoute exact path='/findGroup' component={GroupsSearch} />
          <ProtectedRoute exact path='/createReport' component={ReportPage} />
          <ProtectedRoute exact path='/newOpinion' component={Opinion} />
          <ProtectedRoute exact path='/editOpinion' component={EditOpinion} />
          <ProtectedRoute exact path='/demandsInGroup' component={DemandsInGroup}/>
      </Switch>
      <Footer/>
    </Router>
  );
}

export default MainApp;
