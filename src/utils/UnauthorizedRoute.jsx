import { Redirect, Route } from 'react-router-dom';
import accountService from '../services/accountService';

export const UnauthorizedRoute = ({ component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            !accountService.getAuthToken() ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/",
                        state: {from: props.location}
                    }}
                />
            )
        }
    />
);