export function containsDigits(string){
    const regex = /\d/;
    return regex.test(string);
}

export function containsSpetialChars(string){
    const regex = /[`!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?~]/;
    return regex.test(string);
} 

export function containsSpetialCharsForPassword(string){
    const regex = /[-`!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?~]/;
    return regex.test(string);
} 

export function hasUppercase(srting){
    const regex = /[A-Z]/;
    return regex.test(srting);
}

export function hasLowercase(srting){
    const regex = /[a-z]/;
    return regex.test(srting);
}

