import { isBefore, isEmail } from "validator";
import {containsDigits, containsSpetialChars, containsSpetialCharsForPassword, hasLowercase, hasUppercase} from './utils'

export const requiredError = (value) => {
    if (!value) {
        return (
            <div className="error">Pole jest wymagane.</div>
        );
    }
};

export const dateError = (value) => {
    if(isBefore(value)){
        return (
            <div className="error">Data przewozu powinna być późniejsza niż dzisiejsza.</div>
        );
    }
};

export const firstAndLastNameError = (value) => {
    if (value.length < 1) {
        return (
            <div className="error">Imię nie może być puste.</div>
        );
    }
    else if(containsDigits(value) || containsSpetialChars(value) || value.substr(value.length - 1) === "-" || value.charAt(0) === "-"){
        return (
            <div className="error">Wartość nie może zawierać liczb oraz znaków specjalnych.</div>
        );
    }
};

export const emailError = (value) => {
    if (!isEmail(value)) {
        return (
            <div className="error">Email jest niepoprany.</div>
        );
    }
};

export const passwordError = (value) => {
    if (value.length < 8) {
        return (
            <div className="error">Hasło ma składać się z minimalnie 8 znaków.</div>
        );
    }
    else if(value.length > 40){
        return (
            <div className="error">Hasło nie może przekraczać długości 40 znaków.</div>
        );
    }
    else if(!containsDigits(value) || !containsSpetialCharsForPassword(value)){
        return (
            <div className="error">Hasło musi zawierać przynajmniej jedną cyfrę i znak specjalny.</div>
        );
    } 
    else if(!hasUppercase(value)){
        return (
            <div className="error">Hasło musi zawierać przynajmniej jedną dużą literę.</div>
        );
    }
    else if(!hasLowercase(value)){
        return (
            <div className="error">Hasło musi zawierać przynajmniej jedną małą literę.</div>
        );
    }
};