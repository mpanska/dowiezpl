import { useState } from "react";
import opinionService from "../services/opinionService";
import Rating from '@mui/material/Rating';
import { useHistory, useLocation } from "react-router";

function Opinion() {
    const location = useLocation();
    const history = useHistory();
    const [ value, setValue ] = useState(0);
    const [ description, setDescription ] = useState("");

    const { creator } = location.state ? location.state : {};

    const handleOpinionPublication = () => {
        if(value && creator.accountId){   
            let data = {
                rating: value,
                description: description,
                ratedId: creator.accountId
            };

            opinionService.publishOpinion(data).then(
                response => {
                    history.push("/myDemands");
                },
                error => {
                    console.log(error);
                }
            )
        }
    }

    return (
        <div className="page-with-footer page-content-standard">
            <div className="white-card">
                <h3 className="bold">Dodaj opinię</h3>
                <div className="flex-row-align-center" style={{marginTop: "1rem", marginBottom: "1rem"}}>
                    <p style={{marginRight: "0.7rem"}}>Ocena: </p>
                    <Rating
                        size="large"
                        name="simple-controlled"
                        value={value}
                        onChange={(event, newValue) => { setValue(newValue) }}
                    />
                </div>
                <div style={{marginBottom: "1rem"}}>
                    <textarea className="form-input-unset" value={description} onChange={e => setDescription(e.target.value)} name="opinion" id="opinion" cols="120" rows="5" maxLength="2000" required></textarea>
                </div>
                <button className="basic-button" onClick={handleOpinionPublication}>Dodaj opinię</button>
            </div>
        </div>
    );
}

export default Opinion;