import React from 'react';
import Logo from '../components/elements/Logo';

function PrivacyPolicy() {
    return (
        <div className="page-content-standard">
            <Logo/>
            <div className="white-card" style={{marginTop: "2rem"}}>
                <h1 style={{textAlign: "center"}} className="bold">Polityka prywatności</h1>
                <h2>1. Informacje ogólne</h2>
                <p>
                    Dana polityka prywatności systemu dowiez.pl ma charakter informacyjny, nie jest ona źródłem obowiązków dla klientów aplikacji.
                </p>
                <p>
                    Dane osobowe Klienta są przetwarzane zgodnie z ustawą o ochronie danych osobowych
                    (Dz.U. 1997 Nr 133, poz. 883 ze zm.) oraz ustawą o świadczeniu 
                    usług drogą elektroniczną (Dz.U. 2002 Nr 144, poz. 1204 ze zm.).
                </p>
                <h2>2. Cel i zakres zbierania danych osobowych</h2>
                <p>
                    System wymaga następujących danych osobowych w celu założenia konta: nazwisko, imię, adres poczty elektronicznej.
                </p>
                <p>
                    System nie wymaga i nie zbiera żadnych danych poufnych, takich jak np. dane kont bankowych, 
                    kart kredytowych, numeru PESEL czy adresu zamieszkania.
                </p>
                <p>
                    Dane przechowywane w systemie mogą być wykorzystywane przez moderatorów systemu dowiez.pl w celu rozpatrzenia zgłoszenia lub w celach statystycznych. Żadne dane nie są przekazywane do osób trzecich.</p>
                <h2>3. Pliki Cookies</h2>
                <p>
                    Pliki cookies w systemie dowiez.pl używane są tylko do utrzymywania sesji poprzez wygenerowanie i odesłanie tymczasowego tokena po logowaniu, który wygasa po 30 dniach.
                </p>
                <h2>4. Bezpieczeństwo</h2>
                <p>
                    Wszystkie hasła są zabezpieczone i przechowywane w postaci zahaszowanej. Nawet osoby uprawnione, 
                    mające dostęp do bazy danych, nie odczytają rzeczywistego hasła.
                </p>
                <p>
                    Nie należy przekazywać osobom trzecim własne dane do logowania w systemie.
                </p>
                <p>
                    Hasło musi spełniać pewne wymogi bezpieczeństwa. Powinno ono mieć długość minimalną 8 znaków, zawierać cyfrę, znak specjalny, co najmniej jedną oraz dużą literę.
                </p>
            </div>
        </div>
    );
}

export default PrivacyPolicy;