import * as utils from "../utils/validations";
import validator from "validator";
import { useRef, useState } from 'react';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { useLocation } from 'react-router';
import accountService from "../services/accountService";
import Alert from '@mui/material/Alert';
import close_icon from "../images/close_btn.svg"; 

function EditUserData() {
    const location = useLocation();
    const { currentUserData }  = location.state;

    const [email, setEmail] = useState(currentUserData.email);
    const [firstName, setFirstName] = useState(currentUserData.firstName);
    const [lastName, setLastName] = useState(currentUserData.lastName);

    const [changePassword, setChangePassword] = useState(false);
    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [passwordMissmatch, setPasswordMissmatch] = useState(false);
    const [missmatchError, setMissmatchError] = useState(false);
    const [passwordSuccedd, setPasswordSuccess] = useState(false);
    const [dataSuccess, setDataSuccess] = useState(false);

    const formRef = useRef();
    const passwordFormRef = useRef();
    const checkPwordButtonRef = useRef();
    const checkErrorsButtonRef= useRef();

    const handleSubmit = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            let newData = {
                accountId: currentUserData.accountId,
                email: email,
                firstName: firstName,
                lastName: lastName,
            };

            accountService.editUserData(newData).then(
                response =>{
                    setDataSuccess(true);
                },
                error => {
                    console.log(error)
                }
            )
        }
    }

    const handlePasswordChange = () => {
        setChangePassword(true)  
    }

    const handleDataChange = () => {
        setChangePassword(false)  
    }

    const handleNewPassword = (e) => {
        e.preventDefault();
        passwordFormRef.current.validateAll();

        if(confirmPassword !== newPassword){
            setPasswordMissmatch(true);
            return;
        }
        else{
            setPasswordMissmatch(false);
        }
        
        if(confirmPassword === newPassword && checkPwordButtonRef.current.context._errors.length === 0) {
            let newData = {
                oldPassword: oldPassword,
                newPassword: newPassword
            }
            accountService.changePassword(newData).then(
                response =>{
                    setPasswordSuccess(true);
                },
                error => {
                    if (error.response) {
                        setMissmatchError(true);
                    }
                }
            )
        }
    }

    const handleSuccessClose = () => {
        setPasswordSuccess(false);
        setDataSuccess(false);
    }

    return (
        <div className="page-with-footer">
            <div className="page-content-standard white-card">
                <div className="flex-row-spaced">
                    {changePassword ? 
                        <div>
                            <h2 className="bold">Zmiana hasła</h2>
                            <p>Żeby zatwierdzić zmianę hasła wprowadź aktualne hasło</p>
                        </div> 
                        : 
                        <h2 className="bold">Twoje dane</h2>
                    }
                    
                    <div className="link-p-hover animated-link">
                        {changePassword ? <span onClick={handleDataChange}>Edycja danych</span> : <span onClick={handlePasswordChange}>Zmiana hasła</span>}
                    </div>
                </div>

                { changePassword ? (
                <Form ref={passwordFormRef} onSubmit={e => handleNewPassword(e)}>
                    <div>
                        <div className="login-panel-input">
                            <label className="login-label" htmlFor="oldPassword">Aktualne hasło</label>
                            <Input className={missmatchError ? "error-input" :"form-input"} type="password" id="oldPassword" required
                                value={oldPassword}
                                onChange={e => setOldPassword(e.target.value)}
                                validations={[utils.requiredError]}     
                            />
                            { missmatchError ? <div className="error">Wprowadzono błędne hasło</div> : <div></div>}
                        </div>
                        <hr/>
                        <div className="login-panel-input">
                            <label className="login-label" htmlFor="newPassword">Nowe hasło</label>
                            <Input className="form-input" type="password" id="newPassword" required
                                value={newPassword}
                                onChange={e => setNewPassword(e.target.value)}
                                validations={[utils.requiredError, utils.passwordError]}        
                            />
                        </div>
                        <div className="login-panel-input">
                            <label className="login-label" htmlFor="confirmPassword">Potwierdź hasło</label>
                            <Input className="form-input" type="password" id="confirmPassword" required
                                value={confirmPassword}
                                onChange={e => setConfirmPassword(e.target.value)}
                                validations={[utils.requiredError]}        
                            />
                            { passwordMissmatch ? <div className="error">Nowe hasła są różne</div> : <div></div> }
                        </div>
                    </div>
                    <button style={{marginTop: "2rem"}} className="login-button glowing-button" type="submit">Zmień hasło</button>
                    <CheckButton className="hidden" ref={checkPwordButtonRef}/>
                </Form> 

                ) : (
                <div>
                <Form ref={formRef} onSubmit={e => handleSubmit(e)}>
                    <div>                    
                        <div className="login-panel-input">
                            <label className="login-label" htmlFor="firstName">Imię</label>
                            <Input className="form-input" type="text" id="firstName" required
                                value={firstName}
                                onChange={e => setFirstName(validator.trim(e.target.value))}
                                validations={[utils.requiredError, utils.firstAndLastNameError]}            
                            />
                        </div>
                        <div className="login-panel-input">
                            <label className="login-label" htmlFor="lastName">Nazwisko</label>
                            <Input className="form-input" type="text" id="lastName" required
                                value={lastName}
                                onChange={e => setLastName(validator.trim(e.target.value))}
                                validations={[utils.requiredError, utils.firstAndLastNameError]}               
                            />
                        </div>
                        <div className="login-panel-input">
                            <label className="login-label" htmlFor="email">Email</label>
                            <Input className="form-input" type="email" id="email" required
                                value={email}
                                onChange={e => setEmail(validator.trim(e.target.value))}
                                validations={[utils.requiredError, utils.emailError]}              
                            />
                        </div>
                    </div>
                    <button style={{marginTop: "2rem"}} className="login-button glowing-button" type="submit">Zapisz zmiany</button>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
                </div>
                )}
            </div>
            { passwordSuccedd ? 
                    <Alert className="alert" variant="filled" severity="success"  style={{backgroundColor: "rgb(93 204 97)"}}
                        action={ <img className="close-button"  src={close_icon} onClick={handleSuccessClose} alt="close"/>} sx={{ mb: 2 }}>
                        Hasło zostało zmienione
                    </Alert> : ""
                }
                { dataSuccess ? 
                    <Alert className="alert" variant="filled" severity="success" style={{backgroundColor: "rgb(93 204 97)"}}
                        action={ <img className="close-button"  src={close_icon} onClick={ handleSuccessClose} alt="close"/>} sx={{ mb: 2 }}>
                        Dane zostały zaktualizowane
                    </Alert> : ""
                }
        </div>
    );
}

export default EditUserData;