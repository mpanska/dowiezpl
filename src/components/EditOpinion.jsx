import { Rating } from "@mui/material";
import { useState } from "react";
import { useHistory, useLocation } from "react-router";
import delete_icon from "../images/delete_forever_white.svg";
import opinionService from "../services/opinionService";
import ConfirmDialog from "./elements/ConfirmDialog";
import { Alert } from "@mui/material";
import close_icon from "../images/close_btn.svg"; 

function EditOpinion() {
    const location = useLocation();
    const history = useHistory();
    const { opinionData }  = location.state;
    const [ value, setValue ] = useState(opinionData.rating);
    const [ description, setDescription ] = useState(opinionData.description ? opinionData.description : "");
    const [dataSuccess, setDataSuccess] = useState(false);
    const [dataError, setDataError] = useState(false);
    const [isConfirmOpened, setIsConfirmOpened] = useState(false);
    const dialogHeader = "Usunąć opinię?"

    const handleSuccessClose = () => {
        if(document.getElementById("confirm-dialog").style.display === "none"){
            setIsConfirmOpened(false);
        }
        setDataSuccess(false);
    }

    const handleErrorClose = () => {
        setDataError(false);
    }

    const handleOpinionEdit = () => {
        let newData = {
            opinionId: opinionData.opinionId,
            rating: value,
            description: description
        };
        opinionService.editOpinion(newData).then(
            response =>{
                setDataError(false);
                setDataSuccess(true); 
            },
            error => {
                setDataSuccess(false);
                setDataError(true);
            }
        );
    }

    function handleOpenConfirmDialog(){
        setIsConfirmOpened(true);
        document.getElementById("confirm-dialog").style.display = "block";
    }

    const handleOpinionDelete = () => {
        opinionService.deleteOpinion(opinionData.opinionId).then(
            response => {
                history.push('/');
            },
            error => {
                console.log(error);
            }
        )
    }

    return (
        <div className="page-with-footer page-content-standard">
            <div className="white-card">
                <div id="confirm-container"  className={isConfirmOpened ? "shown" : "hidden"}>
                    <ConfirmDialog onClick={handleOpinionDelete} dialogHeader={dialogHeader}/>
                </div>
                <div className="flex-row-spaced">
                    <h3 className="bold">Edycja opinii</h3>
                    <div onClick={handleOpenConfirmDialog} className="delete-button flex-row-align-center">
                        <img src={delete_icon} alt="delete icon" />
                    </div>
                </div>
                
                <div className="flex-row-align-center" style={{marginTop: "1rem", marginBottom: "1rem"}}>
                    <p style={{marginRight: "0.7rem"}}>Ocena: </p>
                    <Rating
                        size="large"
                        name="simple-controlled"
                        value={value}
                        onChange={(event, newValue) => { setValue(newValue) }}
                    />
                </div>
                <div style={{marginBottom: "1rem"}}>
                    <textarea className="form-input-unset" value={description} onChange={e => setDescription(e.target.value)} name="opinion" id="opinion" cols="100" rows="5" maxLength="2000" required></textarea>
                </div>
                <button className="basic-button" onClick={handleOpinionEdit}>Zapisz zmiany</button>
            </div>
            { dataSuccess ? 
                <Alert className="alert" variant="filled" severity="success" style={{backgroundColor: "rgb(93 204 97)"}}
                    action={ <img className="close-button"  src={close_icon} onClick={ handleSuccessClose} alt="close"/>} sx={{ mb: 2 }}>
                    Dane zostały zaktualizowane
                </Alert>
                : ""
            }
            { dataError ?
                <Alert className="alert" variant="filled" severity="warning"
                    action={ <img className="close-button"  src={close_icon} onClick={ handleErrorClose} alt="close"/>} sx={{ mb: 2 }}>
                    Nie wprowadzono żadnych zmian.
                </Alert> 
                : ""
            }
        </div>
    );
}

export default EditOpinion;