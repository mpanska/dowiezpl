import * as validation from '../utils/validations';
import validator from 'validator';
import { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import registerImage from '../images/undraw_Next_option_left.svg';
import accountService from "../services/accountService";
import Logo from "../components/elements/Logo";

function Register() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [agreement, setAgreement] = useState(false);
    const [success, setSuccess] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [registerError, setRegisterError] = useState(false);
    const [showAgreementError, setShowAgreementError] = useState(false);

    const formRef = useRef();
    const checkErrorsButtonRef = useRef();
    const passwordRef = useRef("");

    const handleRegister = (e) => {
        e.preventDefault();
        setSuccess(false);
        formRef.current.validateAll();

        if(password !== confirmPassword){
            setPasswordError(true);
            return;
        }
        if(!agreement){
            setShowAgreementError(true);
            return;
        }
        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            accountService.createAccount(firstName, lastName, email, password)
            .then(
                response => {
                    setSuccess(true);
                },
                error => {
                    setSuccess(false);
                    setRegisterError(true);
                }
            );
        }
    }

    const handleCheckbox = (e) => {
        if(e.target.checked){
            setAgreement(true);
        }
        else{
            setAgreement(false);
        }
    }

    return (
        <body className="Register">
            <div className="login-panel centered-div flex-row-spaced">
                <div className="login-panel-left">
                    <Logo/>
                    {!success && (
                        <Form ref={formRef} onSubmit={handleRegister}>
                            <div className="inputs-area-smaller">
                                <div className="inputs-area-header">
                                    <h2>Rejestracja</h2>
                                </div>
                                {
                                    registerError ? <div style={{marginTop:"1rem"}} className="error bold">Wystąpił błąd przy próbie założenia konta. Najprawdopodobniej, konto o podanym adresie email już istnieje.</div> : ""
                                }
                                <div className="login-panel-input">
                                    <label className="login-label" htmlFor="firstName">Imię</label>
                                    <Input 
                                        className="form-input" type="text" id="firstName" required
                                        onChange={e => setFirstName(validator.trim(e.target.value))}
                                        validations={[validation.requiredError, validation.firstAndLastNameError]}
                                    />
                                </div>
                                <div className="login-panel-input">
                                    <label className="login-label" htmlFor="lastName">Nazwisko</label>
                                    <Input 
                                        className="form-input" type="text" id="lastName" required
                                        onChange={e => setLastName(validator.trim(e.target.value))}
                                        validations={[validation.requiredError, validation.firstAndLastNameError]}
                                    />
                                </div>
                                <div className="login-panel-input">
                                    <label className="login-label" htmlFor="email">Email</label>
                                    <Input 
                                        className="form-input" type="text" id="email" required
                                        onChange={e => setEmail(validator.trim(e.target.value))}
                                        validations={[validation.requiredError, validation.emailError]}
                                    />
                                </div>
                                <div className="login-panel-input">
                                    <label className="login-label" htmlFor="new-password">Hasło</label>
                                    <Input ref={passwordRef} 
                                        className="form-input" type="password" id="new-password" required
                                        onChange={e => setPassword(e.target.value)}
                                        validations={[validation.requiredError, validation.passwordError]}
                                    />
                                </div>
                                <div className="login-panel-input">
                                    <label className="login-label" htmlFor="passwordRepeat">Powtrórz hasło</label>
                                    <Input 
                                        className={passwordError ? "error-input" : "form-input"} type="password" id="passwordRepeat" required
                                        onChange={e => setConfirmPassword(e.target.value)}
                                        validations={[validation.requiredError]}
                                    />
                                    {passwordError ? <div className="error">Wprowadzone hasła są różne</div> : <div></div>} 
                                </div>

                                <div className="login-options flex-row-align-center">
                                    <div className="flex-row">
                                        <input type="checkbox" className="bigger-checkbox" value={agreement} onChange={e => handleCheckbox(e)}/>
                                        <p style={{margin: "0 0 0 0.5rem"}}>Akceptuję <a className="orange-link" href="/statute" target="_blank"> regulamin </a> i <a className="orange-link" href="/privacyPolicy" target="_blank"> politykę prywarności </a></p>
                                    </div>
                                </div>      
                                { showAgreementError ? <div style={{marginTop: "0.8rem"}} className="error">Przed rejestracją należy zaakceprować regulamin i politykę prywatności</div> : "" }
                                <button className="login-button basic-button" type="submit">Zarejestruj się</button>
                                <p style={{textAlign: 'center'}}>Już masz konto? <a className="orange-link" href="/log-in">Zaloguj się</a></p>
                            </div>
                            <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                        </Form>
                    )}
                    {success && (
                        <div style={{marginTop: "3rem"}}>
                            <p>Wysłaliśmy na adres {email} wiadomość z linkiem do potwierdzenia tego adresu.</p>
                            <p>Skorzystaj z tego linka żeby dokończyć rejestrację.</p>
                            <p>Prosimy sprawdzić zakładkę spam, jeśli wiadomość nie pojawi się w ciągu kilku minut.</p>
                        </div>
                    )}
                </div>
                <div className="login-panel-right">
                    <img src={registerImage} alt="register "/>
                </div>
            </div> 
        </body>
    );
}
  
export default Register;