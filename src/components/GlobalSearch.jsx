import { useEffect, useState } from 'react';
import SearchInput from './elements/SearchInput';
import CategoryFilter from './elements/CategoryFilter';
import CityInput from './elements/CityInput';
import { Link } from 'react-router-dom';
import not_found_img from "../images/undraw_Empty_404.svg";
import user_icon from "../images/user_icon.svg";
import small_icon from "../images/small.svg";
import medium_icon from "../images/medium.svg";
import large_icon from "../images/large.svg";
import other_icon from "../images/other.svg";
import small_car_icon from "../images/small_car.svg";
import big_car_icon from "../images/big_car.svg";
import trailer_car_icon from "../images/trailer_car.svg";
import delivery_car_icon from "../images/delivery_car.svg";

function GlobalSearch(props) {
    const [searchQuery, setSearchQuery] = useState("");
    const [searchCategories, setSearchCategories] = useState("0, 1, 2, 3");
    const [checked , setChecked] = useState([true, true, true, true]);
    const [cityFrom, setCityFrom] = useState(null);
    const [fromCityId, setFromCityId] = useState("");
    const [cityTo, setCityTo] = useState(null);
    const [destinationCityId, setDestinationCityId] = useState("");
    const items = props.itemsList;
    const categoryLabels = props.labels;

    useEffect(() => {
        getAllItems();
    }, [searchCategories, checked, fromCityId, destinationCityId, searchQuery]);
    
    const handleCategoryChange = (e) => {
        if(checked === [false, false, false, false]){
            setSearchCategories("0, 1, 2, 3");
            props.parentSearchCategories("0, 1, 2, 3");
        }
        else if(searchCategories.includes(e)){
            if(searchCategories.indexOf(e) === 0){
                if(searchCategories.length === 1){
                    setSearchCategories(searchCategories.replace(`${e}`, ""));
                    props.parentSearchCategories(searchCategories.replace(`${e}`, ""))
                }
                else{
                    setSearchCategories(searchCategories.replace(`${e}, `, ""));
                    props.parentSearchCategories(searchCategories.replace(`${e}, `, ""))
                }
            }
            else{
                setSearchCategories(searchCategories.replace(`, ${e}`, ""));
                props.parentSearchCategories(searchCategories.replace(`, ${e}`, ""))
            }
            let tempChecked = checked;
            tempChecked[parseInt(e)] = false;
            setChecked(tempChecked);
        }
        else{
            if(searchCategories.length === 0){
                setSearchCategories(searchCategories.concat(`${e}`));
                props.parentSearchCategories(searchCategories.concat(`${e}`))
            }
            else{
                setSearchCategories(searchCategories.concat(`, ${e}`));
                props.parentSearchCategories(searchCategories.concat(`, ${e}`))
            }
            let tempChecked = checked;
            tempChecked[parseInt(e)] = true;
            setChecked(tempChecked);
        }
    }
    
    function handleQueryChange(newValue) {
        if(newValue === "" || newValue === null){
            getAllItems();
            setSearchQuery("");
        }
        else{
            setSearchQuery(newValue);
        }
    }

    function setItems(newValue){
        props.setItems(newValue);
    }

    function getAllItems(){
        props.getAllItems()
    }
    
    const handleSearch = () => {
        let tempItems = [];
        for(var i = 0; i < items.length; i++){
            if(items.at(i)[props.fromLabel] !== null){
                if(items.at(i)[props.fromLabel].cityName.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()) ||
                items.at(i)[props.toLabel].cityName.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()) ||
                items.at(i).description.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()))
                {
                    tempItems.push(items.at(i)) 
                }
            }
            else{
                if(items.at(i)[props.toLabel].cityName.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()) ||
                    items.at(i).description.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()))
                {
                    tempItems.push(items.at(i)) 
                }
            }
        }
        setItems(tempItems);
    }

    const showFilterMobile = () => {
        let filters = document.getElementById("filters-area")
        filters.style.display === "block" ? filters.style.display = "none" : filters.style.display = "block";
    }

    return (
        <div className="search-page flex-row-only-spaced page-content page-with-footer">
            <div className="filters-area">
                <h2 className="bold" style={{marginBottom: "1rem"}}>Filtry</h2>
                <CategoryFilter onChange={handleCategoryChange} checked={checked} labels={categoryLabels}/>
                <div className="login-panel-input">
                    <label className="" htmlFor="setCityFrom">Z miasta</label>
                    <CityInput className="form-input" id="setCityFrom" value={cityFrom}
                        onChange={city => {
                            setFromCityId(city !== null ? city.cityId : '');
                            props.parentFromCity(city !== null ? city.cityId : '')
                            setCityFrom(city);
                        }}
                    />
                </div>
                <div className="login-panel-input">
                    <label className="" htmlFor="setCityTo">Do miasta</label>
                    <CityInput className="form-input" id="setCityTo" value={cityTo}
                        onChange={city => {
                            setDestinationCityId(city !== null ? city.cityId : '');
                            props.parentDestinationCity(city !== null ? city.cityId : '')
                            setCityTo(city);
                        }}
                    />
                </div>
            </div>

            <div className="flex-column search-area" >
                <div className="search-input-area flex-row-align-center">
                    <SearchInput value={searchQuery} onChange={handleQueryChange} onClick={handleSearch}/>
                    <div className="filters" onClick={showFilterMobile}>
                        <p className="glowing-button filter-mobile-btn">filtry</p>
                    </div>
                </div>
                <div className="flex-column search-list-items">
                    <div className="filters-area" id="filters-area">
                        <h2 className="bold" style={{marginBottom: "1rem"}}>Filtry</h2>
                        <CategoryFilter onChange={handleCategoryChange} checked={checked} labels={categoryLabels}/>
                        <div className="login-panel-input">
                            <label className="" htmlFor="setCityFrom">Z miasta</label>
                            <CityInput className="form-input" id="setCityFrom" value={cityFrom}
                                onChange={city => {
                                    setFromCityId(city !== null ? city.cityId : '');
                                    props.parentFromCity(city !== null ? city.cityId : '')
                                    setCityFrom(city);
                                }}
                            />
                        </div>
                        <div className="login-panel-input">
                            <label className="" htmlFor="setCityTo">Do miasta</label>
                            <CityInput className="form-input" id="setCityTo" value={cityTo}
                                onChange={city => {
                                    setDestinationCityId(city !== null ? city.cityId : '');
                                    props.parentDestinationCity(city !== null ? city.cityId : '')
                                    setCityTo(city);
                                }}
                            />
                        </div>
                    </div>
                {
                    items.length === 0 ? (
                    <div className="flex-column" style={{marginTop: "1.6rem"}}>
                        <p className="bold">Nie znaleziono żadnej oferty odpowiadającej parametrom wyszukowania</p>
                        <img style={{width: "30rem", marginTop: "2rem"}} src={not_found_img} alt="not found" />
                    </div>
                    ) : (
                    items.map(
                        item =>
                        <div key={item.id} className="search-list-card">
                            <Link to={ (item.demandId !== null && item.demandId !== undefined) ? `/demand/${item.demandId}` : 
                                (item.transportId !== null && item.transportId !== undefined) ? `/transport/${item.transportId}` : ''}>
                                <div className="card-text">
                                    <div className="card-text-upper">
                                        <div className="flex-row-spaced">
                                        <h2>{item[props.fromLabel] !== null ? item[props.fromLabel].cityName + " - " : ''}{item[props.toLabel].cityName}</h2>
                                        <img src={user_icon} alt="user icon"/>
                                    </div>
                                    <div className="wrapper">
                                        <p>{item.description}</p>
                                    </div>
                                    
                                </div>
                                <div className="card-text-date flex-row-spaced">
                                {item.transportDate ?
                                    <p>Data przewozu: {item.transportDate.split("T")[0]}</p> :
                                    <p>Opublikowano: {item.creationDate  ? item.creationDate.split("T")[0] : ""}</p>
                                }
                                    <img src={ 
                                        (item.demandId !== null && item.demandId !== undefined) ? (
                                            item.category === "Small" ? small_icon : 
                                            item.category === "Medium" ? medium_icon :
                                            item.category === "Big" ? large_icon : 
                                            item.category === "Other" ? other_icon : ''
                                        ) :  
                                        ((item.transportId !== null && item.transportId !== undefined) ? (
                                            item.category === "Small" ? small_car_icon : 
                                            item.category === "Big" ? big_car_icon :
                                            item.category === "WithTrailer" ? trailer_car_icon : 
                                            item.category === "DeliveryCar" ? delivery_car_icon : ''
                                        ) : ('')
                                        )
                                    } alt={item.category} />
                                </div>
                            </div>
                            </Link>
                        </div>
                    )
                )}
            </div>
        </div>

        <div style={{position: "relative"}}>
            <div className="ads">
                <h2>REKLAMA</h2>
            </div>
        </div>
    </div>
    );
}

export default GlobalSearch;