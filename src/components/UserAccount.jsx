import accountService from "../services/accountService";
import { useState } from "react";
import { NavLink } from "react-router-dom";
import user_icon from "../images/user_icon.svg";
import useDeepCompareEffect from "use-deep-compare-effect";

function UserAccount() {
    const [currentUserData, setCurrentUserData] = useState({});

    useDeepCompareEffect(() => {
        getCurrentUserData();
    }, [currentUserData])

    const getCurrentUserData = () => {
        let userData = Promise.resolve(accountService.getCurrentUserData());
        userData.then(
            response => {
                setCurrentUserData(response);
            },
            error => {
                console.log("error with getCurrentUserData " + error.message);
            }
        )
    }

    const handleLogout = () => {
        accountService.logOut();
    }

    return (
        <div className="UserAccount page-with-footer page-content-standard">
            <div className="account-main-card flex-row-space-between">
                <div className="account-main-card-left">
                    <h2>Dane konta</h2>

                    <div className="flex-row-align-center">
                        <div className="user-img-container"><img src={user_icon} alt="user icon" /></div>
                        <div>
                            <p style={{fontWeight: "600", marginBottom: '0'}}>
                                {currentUserData ? currentUserData.firstName : ""} {currentUserData ? currentUserData.lastName : ""}
                            </p>
                            <p style={{marginTop: '0'}}>{currentUserData ? currentUserData.email : ""}</p>
                        </div>
                    </div>
                </div>

                <div className="account-main-card-right">
                    <div>
                        <NavLink to={{pathname: "/editUserData",  state: {currentUserData: currentUserData} }}>
                            <button className="glowing-button" type="button">Edycja danych</button>
                        </NavLink>
                    </div>
                    <div className="link-p-hover animated-link logout-link" >
                        <p onClick={handleLogout}>Wyloguj się</p>
                    </div>
                </div>
            </div>

            <div className="account-main-card flex-row-spaced-even">
                <div className="flex-column">
                    <div className="glowing-button">
                        <a href="/createDemand">Utwórz zapotrzebowanie</a>
                    </div>
                    <div className="link-p-hover animated-link" style={{marginTop: "2rem"}}>
                        <a href="/myDemands">Twoje zapotrzebowania</a>
                    </div>
                    
                </div>
                <div className="flex-column">
                    <div className="glowing-button">
                        <a href="/createTransport">Utwórz Przewóz</a>
                    </div>
                    <div className="link-p-hover animated-link" style={{marginTop: "2rem"}}>
                        <a href="/myTransports">Twoje przewozy</a>
                    </div>
                </div>
            </div>
        </div>
    );
}
    
export default UserAccount;