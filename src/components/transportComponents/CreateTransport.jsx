import { useState, useRef } from "react";
import transportService from "../../services/transportService";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import CityInput from "../elements/CityInput";
import { useHistory } from "react-router";
import { requiredError, dateError } from "../../utils/validations";

function CreateTransport() {
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [cityFrom, setCityFrom] = useState(null);
    const [startsInCityId, setStartsInCityId] = useState("");
    const [cityTo, setCityTo] = useState(null);
    const [endsInCityId, setEndsInCityId] = useState("");
    const [transportDate, setTransportDate] = useState("");

    const formRef = useRef();
    const checkErrorsButtonRef = useRef();
    const history = useHistory();

    const [endsInError, setEndsInError] = useState(false);
    const [startsInError, setstartsInError] = useState(false);

    const handleTransportCreation = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if(endsInCityId === "" && startsInCityId === ""){
            setEndsInError(true);
            setstartsInError(true);
            return;
        }
        else if(endsInCityId === ""){
            setEndsInError(true);
            return;
        }
        else if(startsInCityId === ""){
            setstartsInError(true);
            return;
        }

        let transportData = { 
            transportDate: transportDate,
            description: description,
            category: category,
            startsInCityId: startsInCityId,
            endsInCityId: endsInCityId
        };

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            transportService.createTransport(transportData).then(
                response => {
                    history.push('/myTransports');
                },
                error => {
                    console.log("error with tr " + error);
                }
            )
        }
    }

    return (
        <div className="CreateTransport">
            <div className="create-form">
                <Form ref={formRef} onSubmit={handleTransportCreation}>
                    <section>
                        <h2>Wypełnij dane przewozu</h2>
                        <p>Podaj termin w którym będzie zeralizoany przewóz</p>
                        <div className="login-panel-input">
                            <label htmlFor="transportDate">Początek</label>
                            <Input type="date" className="form-input-width-44" id="transportDate" required
                                onChange={e => setTransportDate(e.target.value)}
                                validations={[requiredError, dateError]}
                            />
                        </div>
                    </section>
                    <hr />
                    <section>
                        <div className="flex-row">
                            <div className="login-panel-input">
                                <label htmlFor="setCityFrom">Punkt początkowy</label>
                                <CityInput className="form-input" id="setCityFrom" isRequired={true}
                                    value={cityFrom}
                                    onChange={city => {
                                        setStartsInCityId(city !== null ? city.cityId : '');
                                        setCityFrom(city);
                                    }} 
                                />
                                {startsInError ? <div className="error">Pole jest wymagane.</div> : <div></div>}
                            </div>
                            <div className="login-panel-input" style={{marginLeft: "10%"}}>
                                <label htmlFor="setCityTo">Punkt końcowy</label>
                                <CityInput className="form-input" id="setCityTo" isRequired={true}
                                    value={cityTo}
                                    onChange={city => {
                                        setEndsInCityId(city !== null ? city.cityId : '');
                                        setCityTo(city);
                                    }}
                                />
                                {endsInError ? <div className="error">Pole jest wymagane.</div> : <div></div>}
                            </div>
                        </div>
                    </section>
                    <hr />
                    <section>
                        <div className="login-panel-input description-input">
                            <label htmlFor="description">Opis przewozu</label>
                            <div>
                                <textarea name="description" id="description" cols="30" rows="5" maxLength="2000" 
                                    onChange={e => setDescription(e.target.value)}
                                ></textarea>
                            </div>
                        </div>
                        <div className="login-panel-input">
                            <label htmlFor="transportType">Typ przewozu/samochodu</label>
                            <div>
                                <select className="form-input-width-44" name="transportType" id="transportType" required 
                                    onChange={e => {setCategory(e.target.value)}}
                                    onLoad={e => {setCategory(e.target.value)}}
                                    validations={[requiredError]}
                                >
                                    <option value="" disabled selected hidden>Wybierz typ transportu...</option>
                                    <option value="Small">Samochód osobowy</option>
                                    <option value="Big">Duże auto</option>
                                    <option value="WithTrailer">Z przyczepą</option>
                                    <option value="DeliveryCar">Ciężarówka</option>
                                </select>
                            </div> 
                        </div>
                    </section>
                    
                    <button style={{marginTop: "2rem"}} className="login-button glowing-button" type="submit">Utwórz przewóz</button>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
            </div>
        </div>
    );
}
      
export default CreateTransport;
      