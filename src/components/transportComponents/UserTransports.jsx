import transportService from '../../services/transportService';
import { useState } from 'react';
import UserTransportCard from './UserTransportCard';
import empty_image from "../../images/undraw_Empty_404.svg";
import useDeepCompareEffect from 'use-deep-compare-effect';

function UserTransports() {
    const [userTransports, setUserTransports] = useState([]);
    const [filter, setFilter] = useState("");

    useDeepCompareEffect(() => {
        getUserTransports();
    }, [userTransports]);

    const getUserTransports = () => {
        transportService.getUserTransports().then(
            response => {
                if(response[0].length > 0){
                    setUserTransports(response[0].reverse());
                }
            },
            error => {
                console.log(error.data);
            }
        )
    }
    
    const displayCreatedTransports = () => {
        setFilter("Declared");
    }

    const displayInProgressTransports = () => {
        setFilter("InProgress");
    }

    const displayCanceledTransports = () => {
        setFilter("Canceled");
    }

    const displayFinishedTransports = () => {
        setFilter("Finished");
    }

    return (
        <div>
            <div className="page-with-footer page-content-standard user-items" style={{marginBottom: "1rem"}}>
            { userTransports.length === 0 ? (  
                <div className="flex-column"> 
                    <h2 className="bold">W tej chwili nie masz jeszcze żadnych przewozów</h2>
                    <a href="/createTransport"><p  className="glowing-button">Utwórz Przewóz</p></a>
                    <img style={{width: '480px', marginTop: '3rem'}} src={empty_image} alt="brak przewozów" />
                </div>
                ) : (
                <main className="container-with-cards">
                    <h2>Twoje przewozy</h2>
                    <div className="user-items-container flex-row-spaced-even">
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayCreatedTransports}>Zadeklarowane</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayInProgressTransports}>W trakcie</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayCanceledTransports}>Skasowane</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayFinishedTransports}>Zakończone</span>
                        </div>
                    </div>
                    <div className="user-items-container">
                    { filter !== "" ? (
                        userTransports.map( transport => transport.status === filter ? 
                            <UserTransportCard transportId={transport.transportId} key={transport.transportId}/> : <span/> ) 
                        ) 
                        :
                        (userTransports.map( transport => 
                            <UserTransportCard transportId={transport.transportId} key={transport.transportId}/>)
                        )      
                    }
                    </div>
                </main>
            )
            }
            </div>
        </div>
    );
}

export default UserTransports;