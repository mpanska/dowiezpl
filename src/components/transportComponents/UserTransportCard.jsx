import { useState } from 'react';
import { Link } from 'react-router-dom';
import { isBefore } from "validator";
import transportService from '../../services/transportService';
import useDeepCompareEffect from "use-deep-compare-effect";
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import small_icon from "../../images/small.svg";
import medium_icon from "../../images/medium.svg";
import large_icon from "../../images/large.svg";
import other_icon from "../../images/other.svg";

function UserTransportCard(props) {
    const transportId = props.transportId;
    const [ transport, setTransport ] = useState([]);
    const [ connectedDemands, setConnectedDemands] = useState([]);
    const [ readyToTransport, setReadyToTransport ] = useState(false);

    useDeepCompareEffect(() => {
        getTransport(transportId);
        getDemandsForTransport(transportId);
        checkIfTransportReady();
    }, [connectedDemands, readyToTransport])

    const getTransport = (transportId) => {
        transportService.getTransport(transportId).then(
            response => {
                setTransport(response.data);
            }
        )
    }

    const getDemandsForTransport = (transportId) => {
        let connectedDemands = Promise.resolve(transportService.getDemandsForTransport(transportId));
        connectedDemands.then(
            response => { 
                if(response[0].length > 0){
                    setConnectedDemands(response[0].reverse());
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    const handleAcceptDemand = (demandId) => {
        transportService.acceptDemand(transportId, demandId).then(
            response => {
                window.location.reload();
            },
            error => {
                console.log(error);
            }
        );
    }

    const handleDenyDemand = (demandId) => {
        transportService.denyDemand(transportId, demandId).then(
            response => {
                window.location.reload();
            },
            error => {
                console.log(error);
            }
        );
    }

    function checkIfTransportReady(){
        let now = new Date()
        let nowString = now.getDate() + "-" + (parseInt(now.getMonth()) + 1) + "-" + now.getFullYear(); //https://www.geeksforgeeks.org/how-to-get-current-formatted-date-dd-mm-yyyy-in-javascript/
        let transportDate = `${transport.transportDate}`;    
        if((isBefore(transportDate) || nowString === transportDate) && transport.status === "Declared"){ 
            setReadyToTransport(true);
        }
    }

    const startTransport = () => {
        transportService.startTransport(transportId).then(
            response => {
                window.location.reload();
            },
            error => {
                console.log(error);
            }
        );
    }

    const finishTransport = () => {
        transportService.finishTransport(transportId).then(
            response => {
                window.location.reload();
            },
            error => {
                console.log(error);
            }
        );
    }

    return (
        <div className="items-container-item item-card">
            <div className="flex-row-spaced">
                <div>
                    <div className="flex-row-align-center">
                        <h3>
                            {transport.startsIn !== undefined ? transport.startsIn.cityName : ""} - {transport.endsIn !== undefined ? transport.endsIn.cityName : ""}
                        </h3>
                        <span style={{marginLeft: '2rem'}}>
                            {transport.status === "Declared" ? "Zadeklarowany" :
                                transport.status === "InProgress" ? "W trakcie" :
                                transport.status === "Canceled" ? "Skasowany" :
                                transport.status === "Finished" ? "Zakończony" :
                                transport.status
                            } 
                        </span>
                    </div>
                    <p>Data przewozu: {transport.transportDate !== undefined ? transport.transportDate.split("T")[0] : transport.transportDate}</p>
                    <p>{transport.description}</p> 
                </div>
                {(transport.status === "InProgress") ? 
                    <button className="button-secondary" onClick={finishTransport}>Zakończ przewóz</button> : ""
                }
                {(readyToTransport && connectedDemands.length > 0)  ?
                    <button className="button-success" onClick={startTransport}>Rozpocznij przewóz</button> : ""
                }
                {(transport.status === "Declared" && connectedDemands.length === 0) ? <Link to={{pathname:"/editTransport", state: {transportData: transport}}}><button className="glowing-button">Edycja danych</button></Link> : "" }
            </div>

            <div style={{marginTop: "1.6rem"}}>{ 
                connectedDemands.map( (demand, i) => 
                    <Accordion key={demand.demandId}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                            <Typography>
                                <div className="flex-row"> 
                                    <div>Zapotrzebowanie {1 + i++}</div>
                                    <div style={{marginLeft: "2rem"}}>{demand.status === "Created" ? "Odrzucone" :
                                        demand.status === "Finished" ? "Zakończone" :
                                        demand.status === "Accepted" ? "Zaakceptowane" : 
                                        demand.status === "InProgress" ? "W trakcie" :
                                        demand.status === "TransportProposed" ? "Zaproponowano przewóz" :
                                        demand.status === "TransportRequested" ? "Proszono o przewóz" : demand.status}
                                    </div>
                                </div>
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails> 
                            <div>
                                <p>{demand.description}</p>
                                <div className="flex-row-spaced">
                                    <p>{demand.creationDate ? demand.creationDate.split("T")[0] : ""}</p>
                                    <img  style={{width: "2rem"}} src={
                                        demand.category === "Small" ? small_icon : 
                                        demand.category === "Medium" ? medium_icon :
                                        demand.category === "Big" ? large_icon : 
                                        demand.category === "Other" ? other_icon : ''
                                    } alt="typ zapotrzebowania"/>
                                </div> 
                                { demand.status === "TransportRequested" ? 
                                    <div style={{marginTop: "1rem"}}>
                                        <button  className="button-success" onClick={() => handleAcceptDemand(demand.demandId)}>Potwierdź zapotrzebowanie</button>
                                        <button style={{marginLeft: "2rem"}} className="button-deny" onClick={() => handleDenyDemand(demand.demandId)}>Odrzuć zapotrzebowanie</button>
                                    </div> : "" 
                                }
                            </div>
                        </AccordionDetails>
                    </Accordion> 
                )
            }</div>
            <p>{transport.creationDate === undefined ? "" : "Utworzono: " + transport.creationDate.split("T")[1].split(".")[0]} {transport.creationDate === undefined ? "" : transport.creationDate.split("T")[0]}</p>
        </div>
    );
}

export default UserTransportCard;