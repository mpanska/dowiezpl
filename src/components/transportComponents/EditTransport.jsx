import { useState, useRef, useEffect } from "react";
import transportService from "../../services/transportService";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import CityInput from "../elements/CityInput";
import { useHistory, useLocation } from "react-router";
import { requiredError, dateError } from "../../utils/validations";
import delete_icon from "../../images/delete_forever_white.svg";
import ConfirmDialog from "../elements/ConfirmDialog";

function EditTransport() {
    const location = useLocation();
    const { transportData }  = location.state;

    const [description, setDescription] = useState(transportData.description);
    const [category, setCategory] = useState(transportData.category);
    const [cityFrom, setCityFrom] = useState(transportData.startsIn);
    const [startsInCityId, setStartsInCityId] = useState(transportData.startsIn.cityId);
    const [cityTo, setCityTo] = useState(transportData.endsIn);
    const [endsInCityId, setEndsInCityId] = useState(transportData.endsIn.cityId);
    let date = transportData.transportDate.split("T")[0];
    const [transportDate, setTransportDate] = useState(date);
    const [endsInError, setEndsInError] = useState(false);
    const [startsInError, setstartsInError] = useState(false);
    const [isConfirmOpened, setIsConfirmOpened] = useState(false);

    const formRef = useRef();
    const checkErrorsButtonRef = useRef();
    const history = useHistory();

    const dialogHeader = "Skasować przewóz?";

    useEffect(() => {
        if(document.getElementById("confirm-dialog").style.display === "none"){
            setIsConfirmOpened(false);
        }
    }, [isConfirmOpened]);

    const handleEditTransportData = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if(endsInCityId === "" && startsInCityId === ""){
            setEndsInError(true);
            setstartsInError(true);
            return;
        }
        else if(endsInCityId === ""){
            setEndsInError(true);
            return;
        }
        else if(startsInCityId === ""){
            setstartsInError(true);
            return;
        }

        let data = { 
            transportId: transportData.transportId,
            transportDate: transportDate,
            description: description,
            category: category,
            startsInCityId: startsInCityId,
            endsInCityId: endsInCityId
        };

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            transportService.editTransport(data).then(
                response => {
                    history.push('/account');
                },
                error => {
                    console.log(error.message);
                }
            )
        }
    }

    const handleOpenConfirmDialog = () =>{
        setIsConfirmOpened(true);
        document.getElementById("confirm-dialog").style.display = "block";
    }

    const handleTransportCancel = () => {
        transportService.cancelTransport(transportData.transportId).then(
            response => {
                history.push('/myTransports');
            },
            error => {
                console.log(error.message);
            }
        )
    }

    return (
        <div className="CreateTransport">
            <div id="confirm-container" className={isConfirmOpened ? "shown" : "hidden"}>
                <ConfirmDialog  onClick={handleTransportCancel} dialogHeader={dialogHeader}/>
            </div>
            <div className="create-form">
                <Form ref={formRef} onSubmit={handleEditTransportData}>
                    <section>
                        <div className="flex-row-spaced" style={{justifyContent: "space-between"}}>
                            <h2>Edycja danych przewozu</h2>
                            <div onClick={handleOpenConfirmDialog} className="delete-button flex-row-align-center">
                                <img src={delete_icon} alt="delete icon" />
                            </div>
                        </div>
                    
                        <p>Podaj termin w którym będzie zeralizoany przewóz</p>
                        <div className="login-panel-input">
                            <label className="" htmlFor="setCityFrom">Początek</label>
                            <Input type="date" className="form-input-width-44" id="setCityFrom" required
                                value={transportDate}
                                
                                onChange={e => setTransportDate(e.target.value)}
                                validations={[requiredError, dateError]}
                            />
                        </div>
                    </section>
                    <hr />
                    <section>
                        <div className="flex-row">
                            <div className="login-panel-input">
                                <label className="" htmlFor="setCityFrom">Punkt początkowy</label>
                                <CityInput className="form-input" id="setCityFrom" isRequired={true}
                                    value={cityFrom} 
                                    onChange={city => {
                                        setStartsInCityId(city !== null ? city.cityId : '');
                                        setCityFrom(city);
                                    }} 
                                />
                                {startsInError ? <div className="error">Pole jest wymagane.</div> : <div></div>}
                            </div>
                            <div className="login-panel-input" style={{marginLeft: "10%"}}>
                                <label className="" htmlFor="setCityTo">Punkt końcowy</label>
                                <CityInput className="form-input" id="setCityTo" isRequired={true}
                                    value={cityTo}
                                    onChange={city => {
                                        setEndsInCityId(city !== null ? city.cityId : '');
                                        setCityTo(city);
                                    }}
                                />
                                {endsInError ? <div className="error">Pole jest wymagane.</div> : <div></div>}
                            </div>
                        </div>
                    </section>
                    <hr />
                    <section>
                        <div className="login-panel-input description-input">
                            <label className="" htmlFor="description">Opis przewozu</label>
                            <div>
                                <textarea name="description" id="description" cols="30" rows="5" maxLength="2000" 
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                ></textarea>
                            </div>
                        </div>
                        <div className="login-panel-input">
                            <label htmlFor="transportType">Typ przewozu/samochodu</label>
                            <div>
                                <select className="form-input-width-44" name="transportType" id="transportType" required 
                                    value={category}
                                    onChange={e => {setCategory(e.target.value)}}
                                    onLoad={e => {setCategory(e.target.value)}}
                                    validations={[requiredError]}
                                >
                                    <option value="" disabled selected hidden>Wybierz typ transportu...</option>
                                    <option value="Small">Samochód osobowy</option>
                                    <option value="Big">Duże auto</option>
                                    <option value="WithTrailer">Z przyczepą</option>
                                    <option value="DeliveryCar">Ciężarówka</option>
                                </select>
                            </div> 
                        </div>
                    </section>
                    <button style={{marginTop: "2rem"}} className="login-button glowing-button" type="submit">Zapisz zmiany</button>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
            </div>
        </div>
    );
}
      
export default EditTransport;
      