import { useState } from "react";
import demandService from "../../services/demandService";
import user_icon from "../../images/user_icon.svg";
import transportService from "../../services/transportService";
import { Link } from "react-router-dom";
import small_car_icon from "../../images/small_car.svg";
import big_car_icon from "../../images/big_car.svg";
import trailer_car_icon from "../../images/trailer_car.svg";
import delivery_car_icon from "../../images/delivery_car.svg"
import useDeepCompareEffect from "use-deep-compare-effect";
import { Alert } from "@mui/material";
import close_icon from "../../images/close_btn.svg"; 

function TransportPage(props) {
    const transportId = props.match.params.transportId;
    const [transport, setTransport] = useState({});
    const [userDemands, setUserDemands] = useState([]);
    const [demandsToAttach, setDemandsToAttach] = useState([]);
    const [operationSuccess, setOperationSuccess] = useState(false);
    const [operationError, setOperationError] = useState(false);

    useDeepCompareEffect(() => {
        getTransport();
        getUserDemands();
    }, [transport, userDemands]);

    function getTransport() {
        transportService.getTransport(transportId).then(
            response => { setTransport(response.data) },
            error => { console.log(error) }
        )
    }

    const handleCheckbox = (e, demand) => {
        let temp = demandsToAttach;
        if(e.target.checked){
            temp.push(demand);
            setDemandsToAttach(temp);
        }
        else{
            temp = temp.filter(val => val.demandId !== e.target.value)
            setDemandsToAttach(temp);
        }
    }

    function getUserDemands() {
        if(transport !== null && transport.startsIn !== undefined){
            let demands = Promise.resolve(demandService.getUserDemands());
            demands.then(
                response => { 
                    let tempData = [];
                    response[0].forEach(demand => {
                        if(transport.startsIn !== undefined && transport.endsIn !== undefined){
                            if(demand.from === null){
                                if(demand.destination.cityName === transport.endsIn.cityName && demand.status === "Created"){
                                    tempData.push(demand);
                                }
                            }
                            else if(demand.from !== null && demand.destination !== undefined){
                                if((demand.from.cityName === transport.startsIn.cityName) && (demand.destination.cityName === transport.endsIn.cityName)
                                    && demand.status === "Created"
                                ){
                                    tempData.push(demand);  
                                }
                            }
                        }
                    });
                    setUserDemands(tempData);
                }
            )
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(demandsToAttach.length > 0){
            demandsToAttach.forEach(
                demand => {
                    demandService.reguestATransport(demand.demandId, transportId).then(
                        response => {
                            setOperationError(false);
                            setOperationSuccess(true);
                        },
                        error => {
                            console.log(error);
                            setOperationSuccess(false);
                            setOperationError(true);
                        }
                    )
                }
            )
        }
    }

    const handleSuccessClose = () => {
        setOperationSuccess(false);
    }

    const handleErrorClose = () => {
        setOperationError(false);
    }

    return (
        <div className="page-with-footer">
            { operationSuccess ? 
                <Alert className="alert" variant="filled" severity="success" style={{backgroundColor: "rgb(93 204 97)"}}
                    action={ <img className="close-button"  src={close_icon} onClick={handleSuccessClose} alt="close"/>} sx={{ mb: 2 }}>
                        Prośba o przewóz została wysłana.
                </Alert> : ""
            }
            {
                operationError ? 
                <Alert className="alert" variant="filled"  severity="error" 
                action={ <img className="close-button" src={close_icon} onClick={handleErrorClose} alt="close"/>} sx={{ mb: 2 }}>
                    Nie zaznaczono żadnych zapotrzebowań, które mogą być przetransportowane w ramach danego przewozu.
                </Alert> : ""
            }
            <div className="flex-row-only-spaced page-content-standard" style={{marginBottom: "1rem"}}>
                    <section style={{marginRight: "2rem", height: "fit-content", width: "100%"}} className="white-card container-shadow">
                        <header className="flex-row-spaced">
                            <div>
                                <h2 className="bold flex-row-align-center">{transport.startsIn === undefined ? '' : transport.startsIn.cityName} - {transport.endsIn === undefined ? '' : transport.endsIn.cityName }
                                    <img style={{paddingLeft: "1.3rem"}} src={
                                    transport.category === "Small" ? small_car_icon : 
                                    transport.category === "Big" ? big_car_icon :
                                    transport.category === "WithTrailer" ? trailer_car_icon : 
                                    transport.category === "DeliveryCar" ? delivery_car_icon : ''
                                    } alt="type" />
                                </h2>
                                <span className="secondary-text">Data przewozu: {transport.transportDate ? transport.transportDate.split("T")[0] : ""}</span>
                            </div>
                                <div>
                                    <Link to={{pathname: transport.creator === undefined ? "#" : `/user/${transport.creator.accountId}`, 
                                        state: {creator: transport.creator} }}
                                    >
                                        <img src={user_icon} alt="creator" />
                                    </Link>
                                </div>
                        </header>
                        <p style={{width: "85%"}}>{transport.description}</p>
                        <div className="flex-row-spaced">
                            {userDemands.length > 0 ?
                                <button className="basic-button" onClick={e => handleSubmit(e)}>Zatwierdź</button>
                                : 
                                <div style={{height: "1px", width: "1px", content: " "}}>&nbsp;</div>
                            }
                            <div className="link-p-hover animated-link logout-link">
                                <Link to={{pathname: "/createReport", state: {transportId: transport.transportId} }}>
                                    <p>Zgłoś naruszenie</p>
                                </Link>
                            </div>
                        </div>
                    </section>
                { userDemands.length > 0 ?
                    <section className="fit-content white-card container-shadow">
                        <h2>Twoje zapotrzebowania pasujące do danego przewozu:</h2>
                        <div className="container-with-cards" id="user-transport-container">
                        {
                            userDemands.map( demand => 
                                <div key={demand.demandId} className="fit-content items-container-item">
                                    <div className="flex-row-spaced">
                                        <h3>
                                            {demand.from !== undefined && demand.from !== null  ? `${demand.from.cityName} - ` : ""}{demand.destination !== undefined ? demand.destination.cityName : ""}
                                        </h3>
                                        <input type="checkbox" className="bigger-checkbox"
                                            value={demand.demandId} name={demand.demandId} id={demand.demandId}
                                            onChange={e => handleCheckbox(e, demand)}
                                        />
                                    </div>

                                    <p style={{width: "400px", paddingRight: "2rem"}}>{demand.description}</p>
                                    <p>Utworzono: {demand ? demand.creationDate.split("T")[0] : " " }</p>

                                </div>
                            )
                        } 
                        </div>
                    </section> : ""
                }
            </div>
        </div>
    );
}

export default TransportPage;