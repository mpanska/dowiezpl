
import { useState } from "react";
import useDeepCompareEffect from "use-deep-compare-effect";
import accountService from "../../services/accountService";
import transportService from "../../services/transportService";
import GlobalSearch from "../GlobalSearch";

function Transports() {
  const [transports, setTransports] = useState([]);
  const [searchCategories, setSearchCategories] = useState("0, 1, 2, 3");
  const [fromCityId, setFromCityId] = useState("");
  const [destinationCityId, setDestinationCityId] = useState("");
  const categoryLabels = ["Samochód osobowy", "Duże auto", "Z przyczepą", "Ciężarówka"];

  const [currentUser, setCurrentUser] = useState({});
  const flag = ['a', 'b'];

  useDeepCompareEffect(() => {
    accountService.getAuthToken();
    document.body.style.backgroundColor = "#FFECE1"; 

    getCurrentUserName();
    if(currentUser === undefined){
      window.location.reload();
    }
  }, [currentUser, flag])

  const getCurrentUserName = () => {
    let userData =  Promise.resolve(accountService.getCurrentUserData());
    userData.then(
        response => {             
          setCurrentUser(response)
        },
        error => {
          console.log(error)
        }
    );
  }

  const handleSearchCategoriesCallback = (childData) =>{
    setSearchCategories(childData)
  }

  const handleFromCityIdCallback = (childData) =>{
    setFromCityId(childData)
  }

  const handleDestinationCityIdCallback = (childData) =>{
    setDestinationCityId(childData)
  }

  const getAllTransports = () => {
    let params = {
      categories: searchCategories,
      startCityId: fromCityId,
      endCityId: destinationCityId
    };

    transportService.getAllTransports(params).then(
      response => {
        setTransports(response.data.reverse());
      },
      error => {
        console.log("error happened, " + error)
      }
    )
  }

  function handleSetTransports(newValue){
    setTransports(newValue)
  }


  return (
    <div>
      <GlobalSearch 
        itemsList={transports} 
        setItems={handleSetTransports} 
        getAllItems={getAllTransports}
        labels={categoryLabels} 
        parentSearchCategories={handleSearchCategoriesCallback}
        parentFromCity={handleFromCityIdCallback}
        parentDestinationCity={handleDestinationCityIdCallback}
        fromLabel="startsIn"
        toLabel="endsIn"
      />
    </div>
  );
}
    
export default Transports;
    