import React, { useRef, useState } from 'react';
import reportService from '../services/reportService';
import Form from "react-validation/build/form";
import { requiredError } from "../utils/validations";
import CheckButton from "react-validation/build/button";
import { useLocation } from 'react-router';
import co_working_icon from "../images/co-working-undraw.svg"

const ReportPage = () => {
    const [description, setDescription] = useState("");
    const [reportSent, setReportSent] = useState(false);

    const formRef = useRef();
    const checkErrorsButtonRef = useRef();

    const location = useLocation();
    const { transportId, demandId, groupId, reportedId, opinionId } = location.state;

    const sendReport = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            let realCategory = transportId ? "Transport" : demandId ? "Demand" : groupId ? "Group" : reportedId ? "User" : null;
            let data = { 
                description: description,
                category: realCategory,
                reportedId: reportedId ? reportedId : null,
                reportedTransportId: transportId ? transportId : null,
                reportedDemandId: demandId ? demandId : null,
                reportedGroupId: groupId ? groupId : null,
            }
            reportService.createReport(data).then(
                response => {
                    console.log(response.data);
                    setReportSent(true);
                },
                error => {
                    console.log(error.message);
                }
            );
        }
    }

    return (
        <div className="page-with-footer">
            <div className="page-content-standard">
                { reportSent ? 
                    <div className="white-card flex-column"> 
                        <h2 className="bold">Twoje zgłoszenie zostało wysłane</h2>
                        <p>Dziękujemy za poświęcony czas! W najbliższym czasie moderator rozpatrzy zgłoszenie.</p>
                        <img style={{width: "400px"}} src={co_working_icon} alt="co-work" />
                        <a style={{marginTop: "1rem"}} className="glowing-button" href="/">Powrót do strony głównej</a>
                    </div> 
                    :
                    <div className="white-card">
                        <h2 className="bold">Wypełnij zgłoszenie</h2>
                        <div className="login-panel-input">
                            <p>
                                Kategoria zgłoszenia:  <span>{transportId ? "Przewóz" :  demandId ? "Zapotrzebowanie" : groupId ? "Grupa" : opinionId ? "Opinia" : reportedId ? "Użytkownik" : "Nieznana"}</span>
                            </p>
                        </div>
                        <hr />
                        <Form ref={formRef} onSubmit={e => sendReport(e)}>
                            <div className="login-panel-input">
                                <label className="login-label" htmlFor="newPassword">Opis zgłoszenia</label>
                                <div>
                                    <textarea className="form-input"
                                        name="description" id="description" cols="30" rows="5" maxLength="2000" required 
                                        value={description}
                                        onChange={e => setDescription(e.target.value)}
                                        validations={[requiredError]}
                                    ></textarea>
                                </div>
                            </div>
                            <button style={{marginTop: "2rem"}} className="login-button glowing-button" type="submit">Wyślij zgłoszenie</button>
                            <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                        </Form>
                    </div>
                }
            </div>
        </div>
    );
};

export default ReportPage;