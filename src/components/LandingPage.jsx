import undraw_takeout_boxes from '../images/undraw_takeout_boxes.svg'
import undraw_logistics from '../images/undraw_logistics.svg'
import how_many_users_icon from '../images/how_many_users_icon.svg'
import how_many_localizations_icon from '../images/how_many_localizations_icon.svg'
import how_many_packs_icon from '../images/how_many_packs_icon.svg'
import list_item_check from '../images/list_item_check.svg'

function LandingPage() {
    return (
        <div className="landing-page">
            <div className="landing-page-up flex-row-align-center">
                <div className="landing-page-up-text">
                    <h1>DOWIEZ.PL</h1>
                    <p>
                        Nie jesteśmy firmą kurierską, jesteśmy społecznością! Potrzebujesz dostarczyć szafę do innego miasta? 
                        Chcesz dołączyć do grup przewozowych twojego osiedla? Wolisz stworzyć własną grupę dla wspólnych dostaw gdzie dołączą tylko twoi znajomi?
                        Możesz zrobić to wszystko z dowiez.pl.
                    </p>
                    <a href="/register"><button className="glowing-button">Dołącz do nas</button></a>
                </div>

                <img className="takeout_boxes" src={undraw_takeout_boxes} alt="people holding boxes"/>
            </div>


            <div className="landing-page-middle">
                <div className="landing-page-panel flex-row container-shadow">
                    <div className="landing-page-panel-option flex-row-align-center">
                        <div>
                            <img src={how_many_users_icon} alt="users icon"/>
                        </div>
                        <div>
                            <p className="stats-bold">100+</p>
                            <p>Użytkowników</p>  
                        </div>
                    </div>
                    <div className="vl"></div>
                    <div className="landing-page-panel-option flex-row-align-center">
                        <div>
                            <img src={how_many_localizations_icon} alt="localizations icon"/>
                        </div>
                        <div>
                            <p className="stats-bold">50+</p>
                            <p>Lokalizacji</p>
                        </div>
                    </div>
                    <div className="vl"></div>
                    <div className="landing-page-panel-option flex-row-align-center">
                        <div>
                            <img src={how_many_packs_icon} alt="pack icon"/>
                        </div>
                        <div>
                            <p className="stats-bold">200+</p>
                            <p>Dostarczonego bagażu</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="landing-page-bottom flex-row-align-center">
                <div className="landing-page-bottom-image">
                    <img className="logistics" src={undraw_logistics} alt="people holding boxes"/>
                </div>
                <div className="landing-page-bottom-text">
                    <div className="0">
                        <h1>Tu szybko doręczysz bagaż do przewozu oraz pomożesz go dowieźć!</h1>
                    </div>
                    <div>
                        <div className="flex-row">
                            <img className="check-list-icon" src={list_item_check} alt="check-list-icon"/>
                            <p>Zostań kurierem.</p>
                        </div>
                        <div className="flex-row">
                            <img className="check-list-icon" src={list_item_check} alt="check-list-icon"/>
                            <p>Doręcz bagaż do przewozu.</p>
                        </div>
                        <div className="flex-row">
                            <img className="check-list-icon" src={list_item_check} alt="check-list-icon"/>
                            <p>Przewozy i doręczenia w kręgu znajomych.</p>
                        </div>
                    </div>
                </div>
            </div>

            
            <div className="landing-page-ending container-shadow">
                <div className="landing-page-ending-text">
                    <h2>Dołącz do nas</h2>
                    <p>I opublikuj swoją pierwszą ofertę już dziś</p>
                </div>
                <p class="glowing-button"><a href="/register">Załóż konto</a></p>     
            </div>
        </div>
    );
}
  
export default LandingPage;
  