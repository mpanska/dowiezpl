
import Logo from "./elements/Logo";

function Statute() {
    return (
        <div className="page-content-standard" style={{marginBottom: "2rem"}}>
            <Logo/>
            <div className="white-card" style={{margin: "2rem"}}>
                <h1 className="bold" style={{textAlign: "center", marginBottom: "1rem"}}>Regulamin korzystania z serwisu</h1>
                <h2>1. Informacje ogólne</h2>
                <p>
                    Autorem niniejszej aplikacji webowej jest Marharyta Panska. <br/>
                    Aplikacja została wykonana w ramach kursu Praca dyplomowa na Politechnice Wrocławskiej (2021-2022).
                </p>
                <h2>2. Rodzaje i zakres usług</h2>
                <p> 
                    Serwis dowiez.pl jest systemem przeznaczonym do publikacji ofert przewozu bagażu bądź zapotrzebowań na przewóz pewnych dóbr.
                    Głównymi funkcjami systemu są: 
                    <ul>
                        <li>Przeglądanie, wyszukiwanie ofert przewozu/zapotrzebowań</li>
                        <li>Tworzenie własnych ofert dotyczących przewozu bagażu lub zapotrzebowań na przewóz</li>
                        <li>Tworzenie, modyfikacja i usuwanie własnych grup, możliwość dołączyć/opuścić już istniejącą grupę</li>
                        <li>Zarządzanie własnym kontem</li>
                        <li>Korzystanie z ofert innych klientów</li>
                        <li>Zarządzanie własnymi ofertami</li>
                        <li>Ocenianie klienta, z oferty przewozu którego się skorzystało</li>
                        <li>Tworzenie zgłoszeń nadużyć.</li>
                    </ul>
                </p>
                <h2>3. Zasady korzystania z serwisu</h2>
                <p>Zakładając konto w systemie dowiez.pl klient zobowiązuje się do zapoznania się z oraz do akceptacji danego regulaminu i polityki prywatności.</p>
                <p>Akceptując dany regulamin klient zobowiązuje się do przestrzegania reguł:
                    <ul>
                        <li>Zabronione jest wykonywanie w aplikacji jakichkolwiek działań, które łamią przepisy prawne, obowiązujące na terytorium Polski.</li>
                        <li>Użytkownik zobowiązuje się do publikacji w systemie tylko takich ofert, które dotyczą transportu bagażu oraz zapotrzebowań.</li>
                        <li>Zabroniona jest publikacja w systemie ofert o nieprawdziwej treści.</li>
                        <li>Jeśli klient przyjmuje zapotrzebowanie do przewozu, zobowiązuje się zrealizować zadeklarowany przewóz.</li>
                        <li>Klient realizujący przewóz po odebraniu zapotrzebowania zobowiązuje się do dostarczenia bagażu bez uszkodzeń.</li>
                        <li>Zabronione jest używanie wulgaryzmów.</li>
                        <li>Klient zobowiązuje się szanować i uczciwie traktować innych klientów portalu.</li>
                    </ul>
                </p>
                <h2>4. Odpowiedzialność za niezrealizowanie obowiązków</h2>
                <p>
                    Naruszenie zasad korzystania z serwisu może skutkować:
                    <ul>
                        <li>Usunięciem z systemu oferty/grupy/opinii łamiącej reguły</li>
                        <li>Zablokowaniem konta klienta, który nie dostrzega reguł</li>
                        <li>Za złamanie przepisów prawnych, obowiązujących na terytorium Polski, klient poniesie odpowiedzialność karną.</li>
                    </ul>
                </p>
            </div>
            <div style={{marginBottom: "1rem"}}>&nbsp;</div>
        </div>
    );
}




export default Statute;