import accountService from "../services/accountService";
import LandingPage from "./LandingPage";
import Transports from "./transportComponents/Transports";

function Home() {
    const currentUser = accountService.getAuthToken();
    
    return ( currentUser ? (<Transports />) : (<LandingPage />));
}
    
export default Home;
     