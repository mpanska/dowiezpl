import { useEffect, useState } from "react";
import useDeepCompareEffect from "use-deep-compare-effect";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import groupService from "../../services/groupService";
import SearchInput from "../elements/SearchInput";
import { Link } from "react-router-dom";
import not_found_img from "../../images/undraw_Empty_404.svg";
import close_btn from "../../images/close_btn.svg";
import key_icon from "../../images/key.svg";

function GroupsSearch() {
    const [groups, setGroups] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");
    const [userGroups, setUserGroups] = useState([]);
    const [hasPassword, setHasPassword] = useState(false);
    const [enteredPassword, setEnteredPassword] = useState("");
    const [groupToEnter, setGroupToEnter] = useState({});
    const [wrongPassword, setWrongPassword] = useState(false);

    useDeepCompareEffect(() => {
        handleSearch();
    }, [groups, userGroups]);

    useEffect(() => {}, [searchQuery]);
    
    const getUserGroups = () => {
        let groups = Promise.resolve(groupService.getUserGroups());
        groups.then(
            response => {
                if(response[0].length > 0){
                    setUserGroups(response[0].reverse());
                }
            },
            error => {
                console.log(error);
            }
        )
    }

    const getAllGroups = () => {
        getUserGroups();
        let groupsData = Promise.resolve(groupService.getAllGroups());
        groupsData.then(
            response => {
                if(response[0].length > 0){
                    let rightGroups = [];
                    response[0].reverse().forEach(group => {
                        if(!userGroups.some(g => g.groupId === group.groupId)){
                            rightGroups.push(group)
                        }
                    });
                    setGroups(rightGroups);
                }
            },
            error => {
                console.log(error);
            }
        )
    }

    const joinGroup = (groupObj) => {
        if(!userGroups.some(group => group.groupId === groupObj.groupId)){
            if(groupObj.isPrivate === false){
                let params = {
                    password: null
                };
                handleJoinGroup(groupObj.groupId, params);
            }
            else{
                setHasPassword(true);
                setGroupToEnter(groupObj);
            }
        }
    }

    function handleJoinGroup(groupId, params){
        groupService.joinGroup(groupId, params).then(
            response => {
                window.location.reload();
            },
            error => {
                console.log(error);
                setWrongPassword(true);
            }
        )
    }

    const handleSearch = () => {
        if(searchQuery !== "" && searchQuery !== null){
            let tempItems = [];
            for(var i = 0; i < groups.length; i++){
                if(groups.at(i).name.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()) ||
                    groups.at(i).description.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase()))
                {
                    tempItems.push(groups.at(i));
                }
            }
            setGroups(tempItems);
        }
        else{
            getAllGroups();
        }
    }

    function handleQueryChange(newValue) {
        if(newValue === "" || newValue === null){
            getAllGroups();
            setSearchQuery("");
        }
        else{
            setSearchQuery(newValue);
        }
    }

    const closePopUp = () =>{
        setHasPassword(false);
        setGroupToEnter({});
        setWrongPassword(false);
        setEnteredPassword("");
    }

    const handlePasswordGroup = (e) => {
        e.preventDefault();
        if (groupToEnter) {
            let params = {
                password: enteredPassword 
            };
            handleJoinGroup(groupToEnter.groupId, params);
        }
    }

    return (
        <div className="page-with-footer" >
            <div className="page-content-standard">
                <div className={hasPassword ? "shown" : "hidden"}>
                    <div className="popup">
                        <Form onSubmit={handlePasswordGroup}>
                            <div className="flex-row-spaced">
                                <h2 className="bold">Podaj hasło</h2>
                                <div className="close-button" onClick={closePopUp}><img src={close_btn} alt="close-btn" /></div>
                            </div>
                            <div className="login-panel-input">
                                <label style={{marginBottom: "16px"}} htmlFor="password">Hasło</label>
                                <Input className={wrongPassword === true ? "error-input" : "form-input"} name="password" type="password"
                                    id="password" required
                                    value={enteredPassword}
                                    onChange={e => setEnteredPassword(e.target.value)}
                                />
                                { wrongPassword === true ? <div className="error">Wprowadzono niepoprawne hasło</div> : "" }
                            </div>
                            <button style={{marginTop: "2rem"}} className="login-button basic-button">Dołącz do grupy</button>
                        </Form>
                    </div>
                    <div className="darken-background">&nbsp;</div>
                </div>

                <div className="flex-row-centered" style={{marginBottom: "1rem"}}>
                    <div className="search-input-area">
                        <SearchInput value={searchQuery} onChange={handleQueryChange} onClick={handleSearch}/>
                    </div>
                    <a style={{marginLeft: "4rem"}} href="/myGroups"><button className="basic-button">Powrót do moich grup</button></a>
                </div>

                {groups.length > 0 ? 
                    <div>
                        {groups.map(group =>
                            <div key={group.groupId} className="white-card container-shadow" style={{marginBottom: "1rem"}}>
                                <div className="flex-row-spaced">
                                    <div class="flex-row">
                                        <h2 className="bold">{group.name}</h2>
                                        {group.isPrivate === true ? <img style={{marginLeft: "1.2rem"}} src={key_icon} alt="key-icon"/> : ""}
                                    </div>
                                    <button className="basic-button" onClick={() => joinGroup(group)}>Dołącz do grupy</button>
                                </div>
                                <div className="flex-row-spaced">
                                    <p>{group.description}</p>
                                    <p className="link-p-hover animated-link" style={{marginRight: "2.8rem"}}>
                                        <Link to={{pathname: "/createReport", state: {groupId: group.groupId} }}>Zgłoś grupę</Link>
                                    </p>
                                </div>
                            </div>
                        )}
                    </div>
                    :  
                    <div className="flex-column">
                        <h2 className="bold">Nie znaleziono żadnej grupy</h2>
                        <img style={{width: "30rem", marginTop: "4rem", marginBottom: "3rem"}} src={not_found_img} alt="no new groups" />
                    </div>
                } 
            </div>
        </div>
    );
}

export default GroupsSearch;