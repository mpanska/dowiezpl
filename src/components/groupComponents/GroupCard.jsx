import { Link } from "react-router-dom";
import groupService from "../../services/groupService";
import demandService from "../../services/demandService";
import useDeepCompareEffect from "use-deep-compare-effect";
import { useState } from "react";

function GroupCard(props) {
    const groupData = props.groupData; 
    const currentUser = props.currentUser;
    const [groupDemands, setGroupDemands] = useState([]);

    useDeepCompareEffect(() => {
        getGroupDemands(groupData.groupId);
    }, [groupDemands]);

    const getGroupDemands = (groupId) => {
        let demandsData = {
            categories: "0, 1, 2, 3",
            limitedToGroupId: groupId
        };
        let promise = Promise.resolve(demandService.getAllDemandsArray(demandsData))
        promise.then(
            response => {
                if(response[0].length > 0){
                    setGroupDemands(response[0].reverse());
                }
            },
            error => {
                console.log('error.status=' + error.code);
            }
        )
    }

    const leaveGroup = (group) => {
        groupService.leaveGroup(group.groupId).then(
            response => {
                window.location.reload();
            },
            error => {
                console.log(error);
            }
        )
        
    }

    return (
        <div className="items-container-item white-card container-shadow" style={{marginBottom: "1rem"}}>
            <div className="flex-row-spaced">
                <h2 className="bold">{groupData.name}</h2>
                {((currentUser.accountId && groupData.creator) && (currentUser.accountId === groupData.creator.accountId)) ?
                    <Link to={{pathname:"/editGroup", state: {groupData: groupData}}}>
                        <button className="basic-button">Edytuj grupę</button>
                    </Link>
                    :
                    <button onClick={() => leaveGroup(groupData)} className="button-secondary">Wyjdź z grupy</button>
                }
            </div>
            <p>{groupData.description}</p>
            <div className="flex-row-spaced">
                <span className="secondary-text">Data założenia: {groupData.creationDate ? groupData.creationDate.split("T")[0] : ""}</span>
                <div>{groupDemands.length > 0 ? 
                    <div className="link-p-hover animated-link" >
                        <Link to={{ pathname:"/demandsInGroup", state: { demands: groupDemands } }}><p>Zapotrzebowania w grupie</p></Link>
                    </div>
                    : ""}
                </div>
            </div>               
        </div>
    );
}

export default GroupCard;