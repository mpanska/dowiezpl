import Form from "react-validation/build/form";
import { useRef, useState } from "react";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { requiredError } from "../../utils/validations";
import groupService from "../../services/groupService";
import { useHistory } from "react-router";

function CreateGroup() {
    const history = useHistory();
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [groupPassword, setGroupPassword] = useState("");

    const formRef = useRef();
    const checkErrorsButtonRef = useRef();

    const handleGroupCreation = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        let groupData = {
            name: name,
            description: description,
            groupPassword: groupPassword === "" ? null : groupPassword
        };

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            groupService.createGroup(groupData).then(
                response => {
                    history.push("/myGroups");
                },
                error => {
                    console.log(error)
                }
            );
        }
    }

    return (
        <div className="page-with-footer">
            <div className="page-content-standard white-card">
                <Form onSubmit={e => handleGroupCreation(e)} ref={formRef}>
                    <h2>Wypełnij dane grupy</h2>
                    <div className="login-panel-input">
                        <label htmlFor="groupName">Nazwa grupy</label>
                        <Input className="form-input" type="text" id="groupName" required minLength="4" maxLength="200" 
                            validations={[requiredError]}
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </div>

                    <div className="login-panel-input">
                        <label htmlFor="description">Opis</label>
                        <div>
                            <textarea className="form-input" maxLength="2000" id="description" cols="30" rows="8"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                            />
                        </div>
                    </div>

                    <div className="login-panel-input">
                        <label htmlFor="password">Hasło dostępu</label>
                        <Input className="form-input" type="password" id="password" minLength="4" maxLength="256"
                            value={groupPassword}
                            onChange={e => setGroupPassword(e.target.value)}
                        />
                    </div>

                    <div className="login-panel-input">
                        <button className="login-button glowing-button" type="submit" >Utwórz grupę</button>
                    </div>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
            </div>
        </div>
    );
}

export default CreateGroup;