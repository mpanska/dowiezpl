import { useState } from "react";
import useDeepCompareEffect from "use-deep-compare-effect";
import no_mail_img from "../../images/groups_empty.svg";
import groupService from "../../services/groupService";
import { Link } from "react-router-dom";
import accountService from "../../services/accountService";
import GroupCard from "./GroupCard";

function Groups() {
    const [userGroups, setUserGroups] = useState([]);
    const [currentUser, setCurrentUser] = useState({});

    useDeepCompareEffect(() => {
        getCurrentUserData();
        getUserGroups();
    }, [userGroups]);

    const getUserGroups = () => {
        let groups = Promise.resolve(groupService.getUserGroups());
        groups.then(
            response => {
                if(response[0].length > 0){
                    setUserGroups(response[0].reverse());
                }
            }, 
            error => {
                console.log(error);
            }
        )
    }

    const getCurrentUserData = () => {
        let userData = Promise.resolve(accountService.getCurrentUserData());
        userData.then(
            response => {
                setCurrentUser(response);
            },
            error => {
                console.log("error with getCurrentUserData " + error.message);
            }
        )
    }

    return (
        userGroups.length === 0 ? (
        <div className="page-with-footer">
            <div className="page-content-standard">
                <div className="white-card container-shadow" style={{marginBottom: "1rem", height: "60vh"}}>
                    <div className="flex-row-space-between">
                        <div className="no-messages-text">
                            <div className="flex-column">
                                <h1 className="bold">W tej chwili nie jesteś jeszcze w żadnej grupie</h1>
                                <p style={{width: "600px", textAlign: "center"}}>Twoja lista grup jest obecnie pusta. Jeśli dołączysz do grupy, zobaczysz ją tutaj.
                                    Również możesz samodzienie utworzyć grupę za pomocą poniższego przycisku.
                                </p>
                            </div>
                            <div className="flex-row-spaced-even">
                                <Link to={"/findGroup"} className="glowing-button">Wyszukaj grupę</Link>
                                <p className="bold"> lub </p>
                                <Link to={"/createGroup"} className="glowing-button">Utwórz własną grupę</Link>
                            </div>
                        </div>
                        <div>
                            <img className="no-groups-img" src={no_mail_img} alt="no groups" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ) : (
        <div className="page-with-footer">
            <div className="page-content-standard">
                <div className="items-container" style={{marginBottom: "1rem"}}>
                    <div className="flex-row-spaced">
                        <Link to={"/findGroup"} className="glowing-button">Wyszukiwanie grup</Link>
                        <Link to={"/createGroup"} className="glowing-button">Utwórz nową grupę</Link>
                    </div>
                </div>
                <div>
                    {userGroups.map( group => 
                        <GroupCard groupData={group} currentUser={currentUser}/>
                    )}
                </div>
            </div>
        </div>)
    );
}

export default Groups;