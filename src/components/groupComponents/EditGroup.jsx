import Form from "react-validation/build/form";
import { useRef, useState } from "react";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { requiredError } from "../../utils/validations";
import groupService from "../../services/groupService";
import { useHistory } from "react-router";
import { useLocation } from "react-router";
import delete_icon from "../../images/delete_forever_white.svg";
import ConfirmDialog from "../elements/ConfirmDialog";

function EditGroup() {
    const location = useLocation();
    const groupData = location.state["groupData"];

    const history = useHistory();
    const [name, setName] = useState(groupData.name);
    const [description, setDescription] =  useState(groupData.description);
    const [groupPassword, setGroupPassword] =  useState(groupData.groupPassword);

    const dialogHeader = "Usunąć grupę?";

    const formRef = useRef();
    const checkErrorsButtonRef = useRef();

    const handleGroupEdit = (e) => {
        e.preventDefault();
        formRef.current.validateAll();
        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            let group = {
                groupId: groupData.groupId,
                name: name,
                description: description,
                groupPassword: groupPassword
            }
            groupService.editGroup(group).then(
                response => {
                    history.push('/myGroups');
                },
                error =>{
                    console.log("error.message=" + error.message);
                }
            )
        }
    }

    function handleOpenConfirmDialog(){
        document.getElementById("confirm-container").style.display = "block";
    }

    const handleGropuDelete = () => {
        groupService.deleteGroup(groupData.groupId).then(
            response => {
                history.push('/myGroups');
            },
            error => {

            }
        )
    }

    return (
        <div className="page-with-footer">
            <div className="page-content-standard white-card">
                <div id="confirm-container" className="hidden">
                    <ConfirmDialog onClick={handleGropuDelete} dialogHeader={dialogHeader}/>
                </div>
                <Form onSubmit={e => handleGroupEdit(e)} ref={formRef}>
                    <div className="flex-row-spaced">
                        <h2>Edycja danych grupy</h2>
                        <div onClick={handleOpenConfirmDialog} className="delete-button flex-row-align-center">
                            <img src={delete_icon} alt="delete icon" />
                        </div>
                    </div>
                    
                    <div className="login-panel-input">
                        <label htmlFor="groupName">Nazwa grupy</label>
                        <Input className="form-input" type="text" id="groupName" required minLength="4" maxLength="200" 
                            value={name}
                            onChange={e => setName(e.target.value)}
                            validations={[requiredError]}
                        />
                    </div>

                    <div className="login-panel-input">
                        <label htmlFor="description">Opis</label>
                        <div>
                            <textarea className="form-input" maxLength="2000" id="description" cols="30" rows="8"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                            />
                        </div>
                    </div>

                    <div className="login-panel-input">
                        <label htmlFor="password">Hasło dostępu</label>
                        <Input className="form-input" type="password" id="password" minLength="4" maxLength="256"
                            value={groupPassword}
                            onChange={e => setGroupPassword(e.target.value)}
                        />
                    </div>

                    <div className="login-panel-input">
                        <button className="login-button glowing-button" type="submit" >Zapisz zmiany</button>
                    </div>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
            </div>
        </div>
    );
}

export default EditGroup;