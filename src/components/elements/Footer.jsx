
function Footer() {
  return (
    <div className="Footer">
      <div className="footer-menu">
        <div className="footer-item animated-link">
          <a href="/statute">Regulamin</a>
        </div>
        <div className="footer-item animated-link">
          <a href="/privacyPolicy">Polityka Prywatności</a>
        </div>
      </div>
    </div>
  );
}
  
export default Footer;
  