import { NavLink } from 'react-router-dom';
import logo from '../../images/logo.svg'

function Logo() {
    return (
        <div className="Logo">
            <NavLink to={"/"}>
                <div className="flex-row-align-center navbar-logo">
                    <img src={logo} alt="logo" className="logo"/>
                    <h3>DOWIEZ.PL</h3>
                </div>
            </NavLink>
        </div>
    );
}
  
export default Logo;

  