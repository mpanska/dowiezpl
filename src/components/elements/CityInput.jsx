import { useState, useEffect, useRef } from "react";
import demandService from "../../services/demandService";

function CityInput({value, onChange, inputError, isRequired}) {
    const [citiesList, setCitiesList] = useState([]);
    const [isOpened, setIsOpened] = useState(false);
    const [query, setQuery] = useState("");

    const selectInputRef = useRef(null);

    useEffect(() => {
        ["click", "touchend"].forEach(e => { document.addEventListener(e, toggleSelectOptions)});
        
        getCities();
        return () => ["click", "touchend"].forEach(e => {
            document.removeEventListener(e, toggleSelectOptions);
        });
    }, []);

    const getCities = () => {
        demandService.getCities().then(
            response => {
                setCitiesList(response.data);
            },
            error => {
                console.log(error.data)
            }
        );
    }

    const toggleSelectOptions = (e) => {
        setIsOpened(e && e.target === selectInputRef.current);
    }

    function filterCities(citiesList) {
        return citiesList.filter(
            (city) => (`${city.cityName}`).toLowerCase().indexOf(query.toLocaleLowerCase()) > -1
        );
    }

    function displayValue() { 
        if(query.length > 0) return query;
        if(value) return `${value.cityName}`;
        return "";
    }

    function selectOption(city){
        setQuery("");
        onChange(city);
        setIsOpened(false);
    }
    
    return (
        <div className="CityInput">
            <div className="control">
                <div className={inputError || query ? "error-selected-input" : "selected-value"}>
                    <input type="text" required={isRequired ? isRequired : false}
                        ref={selectInputRef}
                        value={displayValue() || query}
                        
                        onChange={e => {
                            setQuery(e.target.value);
                            onChange(null);
                        }}
                        onClick={() => toggleSelectOptions}
                        onTouchEnd={() => toggleSelectOptions}
                        data-testid="city-input"
                    />
                </div>
                <div className={`arrow ${isOpened ? "open" : ""}`}></div>
            </div>
            <div className={`options ${isOpened ? "open" : ""}`}>
                {
                    filterCities(citiesList).map(
                        city => 
                        <div key={city.id}  className={`option ${value === city ? "selected" : ""}`} 
                            onClick={() => selectOption(city)}
                            onTouchEnd={() => selectOption(city)}
                        >
                            {city.cityName}
                        </div>
                    )
                }              
            </div>
        </div>
    );
}
    
export default CityInput;