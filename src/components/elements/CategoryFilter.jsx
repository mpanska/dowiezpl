import React from 'react';

function CategoryFilter(props) {

  function handleChange(e){
    props.onChange(e.target.value);
  }

  return (
    <div>
      <h3>Kategoria</h3>
      {
        props.labels.map( category =>
          <div key={props.labels.indexOf(category)}>
            <input type="checkbox" 
              name={props.labels.indexOf(category)} 
              id={props.labels.indexOf(category)} 
              value={props.labels.indexOf(category)}
              onChange={handleChange}
              checked={props.checked[props.labels.indexOf(category)]}
            />
            <label htmlFor={props.labels.indexOf(category)}>{props.labels[props.labels.indexOf(category)]}</label>
          </div>
      )}
    </div>
  );
}

export default CategoryFilter;