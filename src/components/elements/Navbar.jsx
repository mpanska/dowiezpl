import { NavLink } from "react-router-dom";
import {  useEffect, useState } from "react";
import accountService from "../../services/accountService";
import user_icon from "../../images/user_icon.svg";
import Logo from "../elements/Logo";
import menu_icon from "../../images/menu_hamburger.svg";
import close_icon from "../../images/close_white.svg"

function Navbar() {
    const userToken = accountService.getAuthToken();
    const [menuOpened, setMenuOpened] = useState(false);
    const [currentUser, setCurrentUser] = useState({});

    useEffect(() => {
        getCurrentUserName();
    },[]);
  
    const getCurrentUserName = () => {
        let userData =  Promise.resolve(accountService.getCurrentUserData());
        userData.then(
            response => {            
                setCurrentUser(response)
            },
            error => {
                console.log(error)
            }
        );
    }

    const handleMobileMenu = () => {
        if(menuOpened === false){
            setMenuOpened(true);
        }
        else{
            setMenuOpened(false);
        }
    }

    return (
        <div className={userToken ? "Navbar" : "NavbarGuest"}>
            <div className="navbar-content">
                {!userToken ? (
                    <div>
                        <div className="navbar-mobile-menu">
                            <img onClick={handleMobileMenu} id="hamburger-icon" src={menuOpened ? close_icon : menu_icon} alt="hamburger-menu" />
                            {menuOpened ? 
                                <div className="navbar-mobile-menu-items flex-row-spaced-even">
                                    <div className="link-p-hover animated-link logout-link" >
                                        <a href="/log-in">Zaloguj się</a>
                                    </div>
                                    
                                    <a href="/register">Rejestracja</a>
                                </div> 
                                : 
                                <div></div> 
                            }
                        </div>
                        <div className="flex-row-align-center navbar-regular-menu" style={{ margin: "0.2em", justifyContent: "space-between" }}>
                            <Logo />
                            <div className="flex-row-align-center">
                                <p style={{ fontWeight: "600", marginRight: "34px" }} className="link-p-hover animated-link">
                                    <a href="/log-in">Zaloguj się</a>
                                </p>
                                <p className="basic-button">
                                    <a href="/register">Rejestracja</a>
                                </p>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>
                        <div className="navbar-mobile-menu">
                            <img onClick={ handleMobileMenu} id="hamburger-icon" src={menuOpened ? close_icon : menu_icon} alt="hamburger-menu" />
                            {menuOpened ? 
                                <div className="navbar-mobile-menu-items flex-row-spaced-even">
                                    <a href="/log-in">Zaloguj się</a>
                                    <a href="/register">Rejestracja</a>
                                </div> 
                                : 
                                <div></div> 
                            }
                        </div>
                        <div className="flex-row-spaced navbar-regular-menu">
                            <Logo />
                            <div className="navbar-nav-links flex-row-align-center">
                                <NavLink to={"/transport"} className="link-hover animated-link"><p>Przewozy</p></NavLink>
                                <NavLink to={"/demand"} className="link-hover animated-link"><p>Zapotrzebowania</p></NavLink>
                                <NavLink to={"/myGroups"} className="link-hover animated-link"><p>Grupy</p></NavLink>
                            </div>
                            <NavLink to={"/account"}>
                                <div className="flex-row-align-center navbar-user-section">
                                    <img className="navbar-user" src={user_icon} alt="user icon" />
                                    <p>{currentUser ? currentUser.firstName : ""}</p>
                                </div>
                            </NavLink>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );

    
}

export default Navbar;
