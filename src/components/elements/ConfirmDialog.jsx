function ConfirmDialog(props) {

    const dialogHeader = props.dialogHeader;

    function handleConfirm(){
        props.onClick();
    }

    function handleClose(){
        document.getElementById("confirm-dialog").style.display = "none";
    }

    return (
        <div id="confirm-dialog" style={{display: "block"}}>
            <div className="popup-confirm">
                <div>
                    <h2 className="bold" style={{marginBottom: "2rem"}}>{dialogHeader}</h2>
                </div>
                <div>
                    <button className="basic-button" onClick={handleConfirm} style={{marginRight: "1.4rem"}}>Tak</button>
                    <button className="button-secondary" onClick={handleClose}>Nie</button>
                </div>
            </div>
            <div className="darken-background">&nbsp;</div>
        </div>
    );
}

export default ConfirmDialog;