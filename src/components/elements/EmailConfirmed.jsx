import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import accountService from '../../services/accountService';

function EmailConfirmed() {
    const history = useHistory();
    const { userId } = useParams();
    const { token } = useParams();

    useEffect(() => {
        let userToken = decodeURIComponent(token)
        accountService.confirmEmail(userId, userToken)
        .then(
            response => {
                history.push('/log-in');
            },
            error => {
                const responseMessage =
                (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
                console.log(responseMessage);
            }
        );
    })

    return (<div></div>);
}

export default EmailConfirmed;