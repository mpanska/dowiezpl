import { useState, useRef } from "react";
import { useHistory } from 'react-router-dom';
import validator from "validator";
import Cookies from 'universal-cookie';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import accountService from "../services/accountService";
import loginImage from '../images/undraw_Next_option_right.svg';
import Logo from "../components/elements/Logo";
import close_btn from "../images/close_btn.svg";
import {requiredError, emailError} from "../utils/validations";

function LoginPage() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loginError, setLoginError] = useState(false);
    const [resetEmail, setResetEmail] = useState("");
    const [passwordResetEmail, setPasswordResetEmail] = useState(false);
    
    useState(() => {
        if(window.location.href.includes("emailForReset")){
            setPasswordResetEmail(true);
        }
    }, [])

    const formRef = useRef();
    const popupRef = useRef();
    const checkErrorsPopupRef = useRef();
    const checkErrorsButtonRef = useRef();
    const history = useHistory();
    const cookie = new Cookies();

    const [forgotPassword, setForgotPassword] = useState(false);

    const handleLogin = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            accountService.logIn(email, password)
            .then(
                response => {
                    let options = { 
                        path: "/", 
                        expires: new Date(Date.parse(response.data.expiration))
                    };
                    cookie.set("token", response.data.token, options);
                    history.push('/transport');
                },
                error => {
                    setLoginError(true);
                }
            )
        }
    }

    const handleForgotPassword = () => { 
        setForgotPassword(true);
    }

    const handlePasswordReset = () => {
        if(forgotPassword && resetEmail !== "" && checkErrorsPopupRef.current.context._errors.length === 0){
            let params = {email: resetEmail}
            accountService.requestResetPassword(params).then( 
                response => {
                    console.log(response.data);
                    console.log("reset");
                    setPasswordResetEmail(true);
                },
                error => {
                    console.log(error.message);
                }
            );
        }
    }

    const closePopUp = () =>{
        setForgotPassword(false);
    }

    return (
        <body className="LoginPage">
            <div className={forgotPassword ? "shown" : "hidden"}>
                <div className="popup">
                    <Form onSubmit={handlePasswordReset} ref={popupRef}>
                        <div className="flex-end" onClick={closePopUp}>
                            <img src={close_btn} alt="close-btn" className="close-button" />
                        </div>
                        
                        <h2 className="bold">Nie pamiętasz hasła?</h2>
                        <p>Podaj swój adres email od konta, do którego chcesz zresetować hasło. Na wskazany email zostanie wysłany kod 
                            do potwierdzenia operacji.
                        </p>
                        <div className="login-panel-input">
                            <label style={{marginBottom: "16px"}} htmlFor="emailForReset">Email</label>
                            <Input className="form-input" name="emailForReset" id="emailForReset" required
                                validations={[requiredError]} 
                                value={resetEmail}
                                onChange={e => setResetEmail(validator.trim(e.target.value))}
                            />
                        </div>
                        <CheckButton className="hidden" ref={checkErrorsPopupRef}/>
                        <button style={{marginTop: "2rem"}} className="login-button basic-button">Zresetuj hasło</button>
                    </Form>
                </div>
                <div className="darken-background">&nbsp;</div>
            </div>

            <div className="login-panel centered-div flex-row-spaced" style={{paddingTop: "2rem"}}>
                <div className="login-panel-left">
                    <Logo/>   
                    <Form onSubmit={handleLogin} ref={formRef}>
                        <div className="inputs-area">
                            { passwordResetEmail ? <p className="bold error">Na wskazany adres email została wysłana wiadomość do potwierdzenia resetu hasła.</p> : ""}
                            <div className="inputs-area-header">
                                <h2>Zaloguj się</h2>
                                <p>Zaloguj się za pośrednictwem danych, które były wprowadzone podczas rejestracji.</p>
                            </div>
                            { loginError ? 
                                <div className="error">
                                    <p>Wprowadzono niepoprawne dane logowania</p>
                                </div> : ""
                            }
                            <div className="login-panel-input">
                                <label className="login-label" htmlFor="email">Email</label>
                                <Input 
                                    className={loginError ? "error-input" : "form-input"} type="text" id="email" required
                                    value={email}
                                    onChange={e => setEmail(validator.trim(e.target.value))}
                                    validations={[requiredError, emailError]}
                                />
                            </div>
                            <div className="login-panel-input">
                                <label className="login-label" htmlFor="password">Hasło</label>
                                <Input 
                                    className={loginError ? "error-input" : "form-input"} type="password" id="password" required
                                    onChange={e => setPassword(e.target.value)}
                                    validations={[requiredError]}
                                />
                            </div>

                            <div className="login-options flex-row-align-center">
                                <span onClick={handleForgotPassword} className="forgot-password">Nie pamiętasz hasła?</span>
                            </div>
                            
                            <div className="centered-button-wrapper">
                                <button className="login-button basic-button" type="submit">Zaloguj się</button>
                            </div>
                            <p style={{textAlign: 'center'}}>Nie masz konta? <a className="orange-link" href="/register">Zarejestruj się</a></p>
                        </div>
                        <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                    </Form>
                </div>
                
                <div className="login-panel-right">
                    <img src={loginImage} alt="login"/>
                </div>
            </div>
        </body>
    );
}
  
export default LoginPage;
  