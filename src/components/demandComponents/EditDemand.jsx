import React, { useRef, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import CityInput from "../elements/CityInput";
import { requiredError } from '../../utils/validations';
import demandService from '../../services/demandService';
import delete_icon from "../../images/delete_forever_white.svg";
import ConfirmDialog from '../elements/ConfirmDialog';
import groupService from '../../services/groupService';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { Alert } from '@mui/material';
import close_icon from "../../images/close_btn.svg"; 

function EditDemand() {
    const location = useLocation();
    const { demandData } = location.state;

    const [description, setDescription] = useState(demandData.description);
    const [category, setCategory] = useState(demandData.category);
    const [cityFrom, setCityFrom] = useState(demandData.from);
    let fromC = demandData.from ? demandData.from.cityId : null;
    const [fromCityId, setFromCityId] = useState(fromC);
    const [cityTo, setCityTo] = useState(demandData.destination);
    const [destinationCityId, setDestinationCityId] = useState(demandData.destination.cityId);
    const [limitedTo, setLimitedTo] = useState(demandData.limitedTo);
    const [userGroups, setUserGropus] = useState([]);
    const [inputValue, setInputValue] = useState("");
    
    const history = useHistory();
    const formRef = useRef();
    const checkErrorsButtonRef = useRef();
    
    const dialogHeader = "Skasować zapotrzebowanie?";

    const [endsInError, setEndsInError] = useState(false);
    const [startsInError, setstartsInError] = useState(false);
    const [updateError, setUpdateError] = useState(false);
    const [confirmOpened, setConfirmOpened] = useState(false);

    useDeepCompareEffect(() => {
        getUserGroups();
        if(document.getElementById("confirm-dialog").style.display === "none"){
            setConfirmOpened(false);
        }
        console.log(inputValue)
    }, [userGroups]);

    const handleEditDemandData = (e) =>{
        e.preventDefault();
        formRef.current.validateAll();

        if(!fromCityId || !destinationCityId){
            console.log(cityFrom)
            if(inputValue !== "" && !fromCityId){
                setstartsInError(true);
            }
            if(!destinationCityId){
                setEndsInError(true);
                return;
            }
        }

        let body = {
            demandId: demandData.demandId,
            description: description,
            category: category,
            fromCityId: fromCityId,
            destinationCityId: destinationCityId,
            recieverUserId: null,
            limitedTo: limitedTo ? limitedTo : null
        };

        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            demandService.editDemand(body).then(
                response => {
                    history.push('/myDemands');
                },
                error =>{
                    setUpdateError(true);
                }
            )
        }
    }

    function handleOpenConfirmDialog(){
        document.getElementById("confirm-dialog").style.display = "block";
        setConfirmOpened(true);
    }

    const handleDemandCancel = () => {
        demandService.cancelDemand(demandData.demandId).then(
            response => {
                history.push('/myDemands');
            },
            error => {
                console.log(error);
            }
        )
    }

    const getUserGroups = () => {
        let groups = Promise.resolve(groupService.getUserGroups());
        groups.then(
            response => {
                if(response[0].length > 0){
                    setUserGropus(response[0].reverse());
                }
            }, 
            error => {
                console.log(error);
            }
        )
    }

    const handleAlertClose = () => {
        setUpdateError(false);
    }

    return (
        <div className="page-with-footer">          
            <div className="white-card page-content-standard" style={{marginBottom: "1rem"}}>
                <div id="confirm-container" className={confirmOpened ? "shown" : "hidden"}>
                    <ConfirmDialog  onClick={handleDemandCancel} dialogHeader={dialogHeader}/>
                </div>

                <Form ref={formRef} onSubmit={e => handleEditDemandData(e)}>
                    <section>
                        <div className="flex-row-spaced">
                            <h2>Edycja danych zapotrzebowania</h2>
                            <div onClick={handleOpenConfirmDialog} className="delete-button flex-row-align-center">
                                <img src={delete_icon} alt="delete icon" />
                            </div>
                        </div>
                        
                        <div className="flex-row">
                            <div className="login-panel-input">
                                <label htmlFor="setCityFrom">Punkt odbioru</label>
                                <CityInput id="setCityFrom" value={ cityFrom }
                                    onChange={city => {
                                        setFromCityId(city !== null ? city.cityId : '');
                                        setCityFrom(city);
                                    }} inputError={startsInError} isRequired={false}
                                ></CityInput>
                                </div>
                                <div className="login-panel-input"  style={{marginLeft: "10%"}}>
                                <label htmlFor="setCityTo">Punkt dostawy</label>
                                <CityInput id="setCityTo" value={cityTo}
                                    onChange={city => {
                                        setDestinationCityId(city !== null ? city.cityId : '');
                                        setCityTo(city);
                                        setInputValue(city);
                                    }}
                                    isRequired={true} inputError={endsInError} 
                                ></CityInput>
                                {endsInError ? <div className="error">Pole jest wymagane.</div> : <div></div>}
                            </div>
                        </div>
                    </section>
                    <hr />
                    <section>
                        <div className="login-panel-input description-input">
                            <label className="" htmlFor="description">Opis zapotrzebowania</label>
                            <div>
                                <textarea className="form-input" name="description" id="description" cols="10" rows="5"  maxLength="2000" 
                                    onChange={e => setDescription(e.target.value)} 
                                    value={description}>
                                </textarea>
                            </div>
                        </div>
                        <div className="login-panel-input">
                            <label htmlFor="demandType">Typ/rozmiar zapotrzebowania</label>
                            <div>
                                <select className="form-input" name="demandType" id="demandType" required style={{width: "80%"}}
                                    onChange={e => {setCategory(e.target.value)}} onLoad={e => {setCategory(e.target.value)}}
                                    validations={[requiredError]} defaultValue={category}
                                >
                                    <option value="Small">Małe</option>
                                    <option value="Medium">Średnie</option>
                                    <option value="Big">Duże</option>
                                    <option value="Other">Niestandardowe</option>
                                </select> 
                            </div>
                        </div>

                        <div className="login-panel-input" style={{marginBottom:"2rem"}}>
                            <label htmlFor="group">Powiąż z grupą</label>
                            <div>
                                <select className="form-input" name="group" id="group" style={{width: "80%"}}
                                    onChange={e => {setLimitedTo(e.target.value)}}>
                                    { !limitedTo ? 
                                        <option value="" disabled selected hidden>Wybierz grupę...</option> : ""
                                    }
                                    { userGroups.map( group => 
                                        <option key={group.groupId} value={group.groupId} selected={limitedTo == group.groupId}>{group.name}</option>
                                    )}
                                </select>
                            </div> 
                        </div>
                        </section>
                    <button className="login-button glowing-button" type="submit">Zapisz zmiany</button>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
            </div>
            { updateError ? 
                <Alert className="alert" variant="filled"  severity="error"  
                    action={ <img className="close-button"  src={close_icon} onClick={ handleAlertClose} alt="close"/>} sx={{ mb: 2 }}>
                    Dane nie mogą być zaktualizowane.
                </Alert> : ""
            }
        </div>
    );
}

export default EditDemand;