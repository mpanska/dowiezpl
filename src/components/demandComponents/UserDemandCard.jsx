import { useState } from 'react';
import demandService from '../../services/demandService';
import { Link } from 'react-router-dom';
import useDeepCompareEffect from 'use-deep-compare-effect';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import opinionService from '../../services/opinionService';

function UserDemandCard(props) {
    const demandId = props.demandId;
    const [ demand, setDemand ] = useState({});
    const [ userOpinions, setUserOpinions ] = useState([]);

    useDeepCompareEffect(() => {
        getDemand();
        getOpinions();
    }, [demand, userOpinions])

    const getDemand = () => {
        let demandData = Promise.resolve(demandService.getDemand(demandId));
        demandData.then(
            response => {
                setDemand(response);
            }
        )
    }

    const getOpinions = () => {
        let otherUserOpinions = Promise.resolve(opinionService.getUserOpinions());
        otherUserOpinions.then(
            response => {
                if(response[0].length > 0){
                    setUserOpinions(response[0]);
                }
            },   
            error => {
                console.log(error);
            }
        )
    }

    const handleTransportAccept = () => {
        demandService.acceptTransport(demandId).then(
            response => {
                window.location.reload();
            },   
            error => {
                console.log(error);
            }
        )
    }

    const handleTransportDeny = () => {
        demandService.denyTransportProposition(demandId).then(
            response => {
                window.location.reload();
            },   
            error => {
                console.log(error);
            }
        )
    }

    return (
        <div>
        { demand ?
        <div className="items-container-item item-card">
            <div className="flex-row-spaced">
                <div>
                    <div className="flex-row-align-center">
                        <h3>{demand.from === undefined || demand.from === null ? '' : `${demand.from.cityName} - `}
                        {demand.destination === undefined ? '' : demand.destination.cityName}</h3>
                        <span style={{marginLeft: '2rem'}}> 
                            {
                                demand.status === "Created" ? "Zadeklarowano" : 
                                demand.status === "TransportRequested" ? "Proszono o przewóz" :
                                demand.status === "TransportProposed" ? "Zaproponowano przewóz" : 
                                demand.status === "Accepted" ? "Zaakceptowane" :
                                demand.status === "InProgress" ? "W trakcie" :
                                demand.status === "Canceled" ? "Skasowane" :
                                demand.status === "Finished" ? "Zakończone" :
                                demand.status
                            } 
                        </span>
                    </div>
                    <p>{demand.description}</p>
                    <div style={{marginTop: "1.6rem"}}>
                    {demand.transport ? 
                        <Accordion style={{width: "fit-content"}}>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                                <Typography>Transport</Typography>
                            </AccordionSummary>
                            <AccordionDetails> 
                                <h3>{demand.transport.startsIn ? demand.transport.startsIn.cityName : ""} - {demand.transport.endsIn ? demand.transport.endsIn.cityName : "" }</h3>
                                <div style={{fontFamily: "'Open Sans', sans-serif"}}>
                                    <p>Data przewozu: {demand.transport.transportDate ? demand.transport.transportDate.split("T")[0] : ""}</p>
                                    <p style={{width: "600px"}}>{demand.transport.description}</p>
                                    <p className="bold">Przewożący: 
                                        <Link style={{marginLeft: "1rem"}} to={{pathname: demand.transport.creator ? `/user/${demand.transport.creator.accountId}` : "#", 
                                            state: {creator: demand.transport.creator, canPublishOpinion: demand.status === "Finished" ? true : false} }} className="link-p-hover animated-link ">
                                            {demand.transport.creator ? demand.transport.creator.firstName + " " + demand.transport.creator.lastName : ""}
                                        </Link>
                                    </p> 
                                </div>
                            </AccordionDetails>
                        </Accordion> : ""                
                    }
                    </div>
                </div>
                { demand.status === "Created" ? <Link to={{pathname:"/editDemand", state: {demandData: demand}}}>
                    <button className="glowing-button">Edycja danych</button>
                    </Link> : <div></div> 
                }
                { demand.status === "TransportProposed" ? 
                    <div className="flex-column">
                        <button onClick={handleTransportAccept} className="button-success" style={{marginBottom: "2rem"}}>Zaakceptuj przwóz</button>
                        <button onClick={handleTransportDeny} className="button-deny">Odrzuć propozycję</button>
                    </div>: ""
                }
            </div>
            <div className="flex-row-spaced">
                <p>Utworzono: { demand.creationDate === undefined ? "" : demand.creationDate.split("T")[1].split(".")[0]} {!demand ? '' : demand.creationDate === undefined ? "" : demand.creationDate.split("T")[0]}</p>
                { demand.status === "Finished" && userOpinions.filter(o => o.rated.accountId === demand.transport.creator.accountId).length === 0 ? 
                    <Link to={{pathname: "/newOpinion", state: {creator: demand.transport.creator }}}><button className="basic-button"><span>Dodaj opinię</span></button></Link> : ""
                }
            </div> 
        </div> : ""
        }
        </div>
    );
}
export default UserDemandCard;