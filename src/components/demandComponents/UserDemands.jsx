import React, { useState } from 'react';
import demandService from "../../services/demandService";
import UserDemandCard from './UserDemandCard';
import empty_image from "../../images/undraw_Empty_404.svg";
import useDeepCompareEffect from 'use-deep-compare-effect';
import accountService from '../../services/accountService';

function UserDemands() {
    const [userDemands, setUserDemands] = useState([]);
    const [filter, setFilter] = useState("");
    const [currentUserId, setCurrentUserId] = useState("");

    useDeepCompareEffect(() => {
        getUserDemands();
        getCurrentUserData();
    }, [userDemands]);

    const getUserDemands = () => {
        demandService.getUserDemands().then(
            response => {
                setUserDemands(response[0].reverse());
            },
            error => {
                console.log(error.data);
            }
        )
    }

    const getCurrentUserData = () => {
        let userData = Promise.resolve(accountService.getCurrentUserData());
        userData.then(
            response => {
                setCurrentUserId(response.accountId);
            },
            error => {
                console.log(error.message);
            }
        )
    }

    const displayCreatedDemands = () => {
        setFilter("Created");
    }

    const displayTransportRequestedDemands = () => {
       setFilter("TransportRequested");
    }

    const displayTransportProposedDemands = () => {
        setFilter("TransportProposed");
    }

    const displayAcceptedDemands = () => {
        setFilter("Accepted");
    }

    const displayInProgressDemands = () => {
        setFilter("InProgress");
    }

    const displayCanceledDemands = () => {
        setFilter("Canceled");
    }

    const displayFinishedDemands = () => {
        setFilter("Finished");
    }

    return (
        <div className="page-with-footer page-content-standard user-items">
        { userDemands.length === 0 ? (
                <div className="flex-column"> 
                    <h2 style={{fontWeight: '600'}}>W tej chwili nie masz jeszcze żadnych zapotrzebowań</h2>
                    <a href="/createDemand"><p className="glowing-button">Utwórz zapotrzebowanie</p></a>
                    <img style={{width: '480px', marginTop: '3rem'}} src={empty_image} alt="brak zapotrzebowań" />
                </div>
            ) : (
                <main className="container-with-cards">
                    <h2>Twoje zapotrzebowania</h2>
                    <div className="user-items-container flex-row-spaced-even">
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayCreatedDemands}>Zadeklarowane</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayTransportRequestedDemands}>Proszono o przewóz</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayTransportProposedDemands}>Zaproponowano przewóz</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayAcceptedDemands}>Zaakceptowane</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayInProgressDemands}>W trakcie</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayCanceledDemands}>Skasowane</span>
                        </div>
                        <div className="link-p-hover animated-link logout-link">
                            <span onClick={displayFinishedDemands}>Zakończone</span>
                        </div>
                    </div>
                    <div className="user-items-container">
                    { filter !== "" ? (
                        userDemands.map( demand => demand.status === filter ? 
                            <UserDemandCard demandId={demand.demandId} userId={currentUserId} key={demand.demandId}/> : <span/> ) 
                        ) 
                        :
                        (userDemands.map( demand => 
                            <UserDemandCard demandId={demand.demandId} userId={currentUserId} key={demand.demandId}/>)
                        )      
                    }
                    </div>
                </main>
            )
        }</div>
    );
}

export default UserDemands;