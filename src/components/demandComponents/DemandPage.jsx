import { useState } from "react";
import demandService from "../../services/demandService";
import user_icon from "../../images/user_icon.svg";
import { Link } from "react-router-dom";
import transportService from "../../services/transportService";
import small_icon from "../../images/small.svg";
import medium_icon from "../../images/medium.svg";
import large_icon from "../../images/large.svg";
import other_icon from "../../images/other.svg";
import useDeepCompareEffect from "use-deep-compare-effect";
import { Alert } from "@mui/material";
import close_icon from "../../images/close_btn.svg"; 

function DemandPage(props) {
    const demandId = props.match.params.demandId;
    const [demand, setDemand] = useState({});
    const [userTransports, setUserTransports] = useState([]);
    const [transportToAttach, setTransportToAttach] = useState([]);
    const [operationSuccess, setOperationSuccess] = useState(false);
    const [operationError, setOperationError] = useState(false);

    useDeepCompareEffect(() => {
        getDemand();
        getUserTransports();
    }, [demand, userTransports]); 

    function getUserTransports(){
        if(demand !== null && demand.from !== undefined){
            let transports = Promise.resolve(transportService.getUserTransports());
            transports.then(
                response => {  
                   var tempData = [];
                    response[0].forEach(transport => {
                        if(demand.from === null){
                            if(demand.destination.cityName === transport.endsIn.cityName && transport.status === "Declared"){
                                tempData.push(transport);
                            }
                        }
                        else {
                            if((demand.from.cityName === transport.startsIn.cityName) && (demand.destination.cityName === transport.endsIn.cityName)
                                && transport.status === "Declared"
                            ){
                                tempData.push(transport);
                            }
                        }
                    });
                    setUserTransports(tempData);
                },
                error => { 
                    console.log(error);
                }
            )
        }

    }

    function getDemand(){
        let prevDemand = Promise.resolve(demandService.getDemand(demandId))
        prevDemand.then(
            response => {
                setDemand(response);
            },
            error => { console.log(error) }
        )
    }

    const handleRadioButton = (e, transport) => {
        if(e.target.checked){
            setTransportToAttach(transport);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(transportToAttach !== null && transportToAttach !== undefined){
            transportService.proposeTransport(transportToAttach.transportId, demandId).then(
                response => {
                    setOperationError(false);
                    setOperationSuccess(true);
                },
                error => {
                    setOperationError(true);
                    setOperationSuccess(false);
                }
            );
        }
    }

    const handleSuccessClose = () => {
        setOperationSuccess(false);
    }

    const handleErrorClose = () => {
        setOperationError(false);
    }

    return (
        <div className="page-with-footer">
            { operationSuccess ? 
                <Alert className="alert" variant="filled" severity="success"  style={{backgroundColor: "rgb(93 204 97)"}}
                    action={ <img className="close-button"  src={close_icon} onClick={handleSuccessClose} alt="close"/>} sx={{ mb: 2 }}>
                        Przewóz został zaproponowany.
                </Alert> : ""
            }
            { operationError ? 
                <Alert className="alert" variant="filled"  severity="error" 
                action={ <img className="close-button"  className="close-button" src={close_icon} onClick={handleErrorClose} alt="close"/>} sx={{ mb: 2 }}>
                    Nie został zaznaczony żadny przewóz lub wybrany przewóz już był zaproponowany wcześniej.
                </Alert> : ""
            }
            <div className="growing-content flex-row page-content-standard">
                <div style={{flexGrow: 12, marginRight: "2rem", height: "fit-content"}}>
                    <section className="white-card container-shadow">
                        <header className="flex-row-spaced">
                            <div>
                                <h2 className="bold flex-row-align-center">{demand.from !== undefined && demand.from !== null ? demand.from.cityName + " - " : ''}{demand.destination === undefined ? '' : demand.destination.cityName}
                                    <img src={
                                        demand.category === "Small" ? small_icon : 
                                        demand.category === "Medium" ? medium_icon :
                                        demand.category === "Big" ? large_icon : 
                                        demand.category === "Other" ? other_icon : ''
                                        } alt="typ zapotrzebowania" style={{paddingLeft: "1.3rem", width: "2rem"}}
                                    />
                                </h2>
                                <span className="secondary-text">Opublikowane: {demand.creationDate ? demand.creationDate.split("T")[0] : ""}</span>
                            </div>
                                <div>
                                    <Link to={{pathname: demand.creator === undefined ? "#" : `/user/${demand.creator.accountId}`, 
                                        state: {creator: demand.creator} }}
                                    >
                                        <img src={user_icon} alt="creator" />
                                    </Link>
                                </div>
                        </header>
                        <p>{demand.description}</p>
                        <div className="flex-row-spaced">
                            { userTransports.length > 0 ?
                                <button className="basic-button" onClick={e => handleSubmit(e)}>Zatwierdź</button> : 
                                <div style={{height: "1px", width: "1px", content: " "}}>&nbsp;</div>
                            }
                            <div className="link-p-hover animated-link logout-link">
                                <Link to={{pathname: "/createReport", state: {demandId: demand.demandId} }}>
                                    <p>Zgłoś naruszenie</p>
                                </Link>
                            </div>
                        </div>
                    </section>
                </div>
            { userTransports.length > 0 ?
                <section className="white-card container-shadow" style={{flexGrow: 1}}>
                    <h2>Twoje prewozy pasujące do danego zapotrzebowania:</h2>
                    <div className="container-with-cards">
                        {userTransports.map( transport => 
                            <div key={transport.transportId} className="items-container-item">
                                <div className="flex-row-spaced">
                                    <h3>
                                        {transport.startsIn !== undefined ? transport.startsIn.cityName : ""} - {transport.endsIn !== undefined ? transport.endsIn.cityName : ""}
                                    </h3>
                                    <input type="radio" 
                                        value={transport.transportId} name="transport" id={transport.transportId}
                                        onChange={e => handleRadioButton(e, transport)}
                                    />
                                </div>
                                <p>{transport.description}</p>
                                <p>Utworzony: {transport.creationDate ? transport.creationDate.split("T")[0] : ""}</p>
                            </div>
                        )}
                    </div>
                </section> : ""
            }   
            </div>
        </div>
    );
}
    
export default DemandPage;