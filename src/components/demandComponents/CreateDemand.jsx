import demandService from "../../services/demandService";
import CityInput from "../elements/CityInput";
import Form from "react-validation/build/form";
import { useState, useRef } from "react";
import { useHistory } from 'react-router-dom';
import CheckButton from "react-validation/build/button";
import {requiredError} from "../../utils/validations";
import groupService from "../../services/groupService";
import useDeepCompareEffect from "use-deep-compare-effect";

function CreateDemand() {
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [cityFrom, setCityFrom] = useState(null);
    const [fromCityId, setFromCityId] = useState("");
    const [cityTo, setCityTo] = useState(null);
    const [destinationCityId, setDestinationCityId] = useState("");
    const [limitedToGroupId, setLimitedToGroupId] = useState("");
    const [userGroups, setUserGropus] = useState([]);

    const history = useHistory(); 
    const formRef = useRef();
    const checkErrorsButtonRef = useRef();

    const [endsInError, setEndsInError] = useState(false);
    const [startsInError, setstartsInError] = useState(false);

    useDeepCompareEffect(() => {
        getUserGroups();
    }, [userGroups]);

    const handleDemandCreation = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if(!fromCityId || !destinationCityId){
            console.log(cityFrom)
            if((cityFrom !== null && cityFrom !== "") && !fromCityId){
                
                setstartsInError(true);
            }
            if(!destinationCityId){
                setEndsInError(true);
                return;
            }
        }

        let demandData = {
            description: description,
            category: category,
            fromCityId: fromCityId,
            destinationCityId: destinationCityId,
            recieverUserId: null,
            limitedToGroupId: limitedToGroupId === "" ? null : limitedToGroupId
        };
        if (checkErrorsButtonRef.current.context._errors.length === 0) {
            demandService.createNewDemand(demandData).then(
                response => {
                    history.push('/myDemands');
                },
                error =>{
                    console.log(error.message);
                }
            )
        }
    }

    const getUserGroups = () => {
        let groups = Promise.resolve(groupService.getUserGroups());
        groups.then(
            response => {
                if(response[0].length > 0){
                    setUserGropus(response[0].reverse());
                }
            }, 
            error => {
                console.log(error);
            }
        )
    }

    
    return (
        <div className="CreateDemand page-with-footer">
            <div className="create-form">
                <Form onSubmit={e => handleDemandCreation(e)} ref={formRef}>
                    <section>
                        <h2>Wypełnij dane zapotrzebowania</h2>
                        <p>Podaj punkt w którym zapotrzebowanie ma być odebrane i gdzie ma być dostrczone </p>
                        <div className="flex-row">
                            <div className="login-panel-input">
                                <label htmlFor="setCityFrom">Punkt odbioru</label>
                                <CityInput className="form-input" id="setCityFrom"
                                    value={cityFrom}
                                    onChange={city => {
                                        setFromCityId(city !== null ? city.cityId : '');
                                        setCityFrom(city);
                                    }}
                                    inputError={startsInError}
                                ></CityInput>
                            </div>
                            <div className="login-panel-input" style={{marginLeft: "10%"}}>
                                <label htmlFor="setCityTo">Punkt dostawy</label>
                                <CityInput id="setCityTo" 
                                    value={cityTo}
                                    onChange={city => {
                                        setDestinationCityId(city !== null ? city.cityId : '');
                                        setCityTo(city);
                                    }}
                                    validations={[requiredError]} inputError={endsInError}
                                ></CityInput>
                                {endsInError ? <div className="error">Pole jest wymagane.</div> : <div></div>}
                            </div>
                        </div>
                    </section>
                    <hr />
                    <section>
                        <div className="description-input">
                            <label htmlFor="description">Opis zapotrzebowania</label>
                            <div>
                                <textarea className="form-input" name="description" id="description" cols="30" rows="5"  maxLength="2000" 
                                    value={description}
                                    onChange={e => setDescription(e.target.value)} 
                                ></textarea>
                            </div>
                        </div>
                        <hr />
                        <div>
                            <label htmlFor="demandType">Typ/rozmiar zapotrzebowania</label>
                            <div>
                                <select style={{width: "80%"}} className="form-input" name="demandType" id="demandType" required
                                    onChange={e => {setCategory(e.target.value)}}
                                    onLoad={e => {setCategory(e.target.value)}}
                                    validations={[requiredError]}
                                >
                                    <option value="" disabled selected hidden>Wybierz typ zapotrzebowania...</option>
                                    <option value="Small">Małe</option>
                                    <option value="Medium">Średnie</option>
                                    <option value="Big">Duże</option>
                                    <option value="Other">Niestandardowe</option>
                                </select>
                            </div> 
                        </div>

                        <div className="login-panel-input" style={{marginBottom:"2rem"}}>
                            <label htmlFor="group">Powiąż z grupą</label>
                            <div>
                                <select style={{width: "80%"}} className="form-input" name="group" id="group"onChange={e => {setLimitedToGroupId(e.target.value)}}>
                                    <option value="" disabled selected hidden>Wybierz grupę...</option>
                                    {
                                        userGroups.map(
                                            group => <option key={group.groupId} value={group.groupId}>{group.name}</option>
                                        )
                                    }
                                </select>
                            </div> 
                        </div>
                    </section>
                    
                    <button className="login-button glowing-button" type="submit" >Utwórz zapotrzebowanie</button>
                    <CheckButton className="hidden" ref={checkErrorsButtonRef}/>
                </Form>
            </div>
        </div>
    );
}
    
export default CreateDemand;
    