import { useLocation } from "react-router";
import { Link } from "react-router-dom";
import user_icon from "../../images/user_icon.svg";
import small_icon from "../../images/small.svg";
import medium_icon from "../../images/medium.svg";
import large_icon from "../../images/large.svg";
import other_icon from "../../images/other.svg";

function DemandsInGroup() {
    const location = useLocation();
    const { demands } = location.state;

    return (
        <div className="page-with-footer page-content-standard">
            <main>
                <h2 className="bold" style={{textAlign: "center"}}>Zapotrzebowania w grupie</h2>
                <div className="user-items-container">
                    {demands.map( demand =>
                        <div key={demand.demandId} className="search-list-card">
                            <Link to={`/demand/${demand.demandId}`}>
                                <div className="card-text">
                                    <div className="card-text-upper">
                                        <div className="flex-row-spaced">
                                            <h3>{demand != undefined && (demand.from === undefined || demand.from === null)  ? '' : `${demand.from.cityName} - `}
                                            {demand != undefined && demand.destination === undefined ? '' : demand.destination.cityName}</h3>
                                            <img src={user_icon} alt="user icon"/>
                                        </div>
                                        <div className="wrapper">
                                            <p>{demand.description}</p>
                                        </div> 
                                    </div>
                                    <div className="card-text-date flex-row-spaced">
                                        <p>{demand.creationDate ? "" : demand.creationDate.split("T")[1].split(".")[0]} {demand.creationDate === undefined ? "" : demand.creationDate.split("T")[0]}</p>
                                        <img src={ 
                                            demand.category === "Small" ? small_icon : 
                                            demand.category === "Medium" ? medium_icon :
                                            demand.category === "Big" ? large_icon : 
                                            demand.category === "Other" ? other_icon : ''
                                        } alt={demand.category} />
                                    </div>
                                </div>
                            </Link>
                        </div>
                    )}
                </div>
            </main>
        </div>
    );
}

export default DemandsInGroup;