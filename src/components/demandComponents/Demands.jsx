import { useState } from "react";
import demandService from "../../services/demandService";
import GlobalSearch from "../GlobalSearch";

function Demands() {
  const [demands, setDemands] = useState([]);
  const [searchCategories, setSearchCategories] = useState("0, 1, 2, 3");
  const [fromCityId, setFromCityId] = useState("");
  const [destinationCityId, setDestinationCityId] = useState("");
  const categoryLabels = ["Małe", "Średnie", "Duże", "Inne"];

  const handleSearchCategoriesCallback = (childData) =>{
    setSearchCategories(childData)
  }

  const handleFromCityIdCallback = (childData) =>{
    setFromCityId(childData)
  }

  const handleDestinationCityIdCallback = (childData) =>{
    setDestinationCityId(childData)
  }

  const getAllDemands = () => {
    let params = {
      categories: searchCategories,
      fromCityId: fromCityId,
      destinationCityId: destinationCityId,
      limitedToGroupId: null
    }
    demandService.getAllDemands(params)
    .then(
      response => {
        setDemands(response.data.reverse());
      },
      error => {
        console.log(error.message);
      }
    )
  }

  function handleSetDemands(newValue){
    setDemands(newValue)
  }

  return (
    <GlobalSearch 
      itemsList={demands} 
      setItems={handleSetDemands} 
      getAllItems={getAllDemands}
      labels={categoryLabels} 
      parentSearchCategories={handleSearchCategoriesCallback}
      parentFromCity={handleFromCityIdCallback}
      parentDestinationCity={handleDestinationCityIdCallback}
      fromLabel="from"
      toLabel="destination"
    />
  );
}
    
export default Demands;
    