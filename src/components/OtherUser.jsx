import { useState } from 'react';
import { useLocation, Link } from 'react-router-dom';
import user_icon from "../images/user_icon.svg";
import Rating from '@mui/material/Rating';
import opinionService from '../services/opinionService';
import useDeepCompareEffect from 'use-deep-compare-effect';


function OtherUser(props) {
    const userId = props.match.params.accountId;
    const location = useLocation();
    const { creator } = location.state ? location.state : {};
    const [ opinions, setOpinions ] = useState([]);
    const [ userOpinions, setUserOpinions ] = useState([]);
    const [ userRating, setUserRating ] = useState(0);

    useDeepCompareEffect(() => {
        getOpinions();
        getMyOpinions();
        calculateUserRating();
    }, [opinions, userOpinions]);
     
    const getOpinions = () => {
        let otherUserOpinions = Promise.resolve(opinionService.getOpinions(userId));
        otherUserOpinions.then(
            response => {
                if(response[0].length > 0){
                    setOpinions(response[0].reverse());
                }
            },   
            error => {
                console.log(error);
            }
        )
    }

    const getMyOpinions = () => {
        let otherUserOpinions = Promise.resolve(opinionService.getUserOpinions());
        otherUserOpinions.then(
            response => {
                if(response[0].length > 0){
                    setUserOpinions(response[0]);
                    filterOpinions();
                }
            },   
            error => {
                console.log(error);
            }
        )
    }

    const filterOpinions = () => {
        opinions.forEach(
            opinion =>{
                userOpinions.map(o => {
                    if(o.opinionId === opinion.opinionId){
                        let first = o.opinionId;
                        let temp = opinions;
                        temp.sort(function(x,y){ return x.opinionId == first ? -1 : y == first ? 1 : 0; });
                        setOpinions(temp);
                        return;
                    }
                })
            }
        )
    }

    function calculateUserRating(){
        if(opinions.length > 0){
            let score = 0;
            opinions.forEach(
                opinion => score += opinion.rating
            )
            setUserRating(score / opinions.length);
        }
        return 0;
    }

    return (
        <div className="page-with-footer page-content-standard">
            <div className="account-main-card flex-row-spaced">
                <div className="flex-row">
                    <div className="user-img-container"><img src={user_icon} alt="user icon" /></div>
                    <div style={{marginLeft: "1rem"}}>
                        <h2>{creator ? creator.firstName : ""} {creator ? creator.lastName : ""}</h2>
                        { userRating > 0 ?
                            <Rating precision={0.5} name="half-rating" value={userRating} readOnly /> : ""    
                        }
                    </div>
                </div>
                <div>
                    <p className="link-p-hover animated-link">
                        <Link to={{pathname: "/createReport", state: {reportedId: userId} }}>Zgłoś użytkownika</Link>
                    </p>
                </div>
            </div>

            {opinions.length > 0 ? 
                <div className="account-main-card" style={{padding: "0px"}}>
                    <div className="white-card container-with-cards">
                        <h2 className="bold" style={{textAlign: "center"}}>Opinie</h2>
                        <div className="flex-row-spaced">
                            <span><strong>Ocena: </strong>{userRating}/5</span>
                            <span>Liczba opinii: {opinions.length}</span>
                        </div>
                        { opinions.map( opinion => 
                            <div key={opinion.opinionId} className="items-container-item">
                                <div className="flex-row-spaced">
                                <Rating name="read-only" value={opinion.rating} readOnly /> 
                                {userOpinions.filter(o => o.opinionId === opinion.opinionId).length > 0 ?
                                        <div className="link-p-hover animated-link">
                                            <Link to={{pathname:"/editOpinion", state: {opinionData: opinion} }}><span>Edytuj opinię</span></Link>
                                        </div> 
                                        :
                                    <div className="link-p-hover animated-link secondary-text">
                                        <Link to={{pathname:"/createReport", state: {opinionId: opinion.opinionId, reportedId: opinion.issuer.accountId} }}><span>Zgłoś opinię</span></Link>
                                    </div> 
                                    }
                                </div>
                                
                                <p style={{marginTop: "0.5rem"}}>{opinion.description}</p>
                                <div className="flex-row-spaced" style={{marginTop: "0.7rem"}}>
                                    <h3>{opinion.issuer ? opinion.issuer.firstName : ""} {opinion.issuer ? opinion.issuer.lastName : ""}</h3>
                                    <span className="secondary-text" style={{margin: "0px"}}>{opinion.creationDate ? opinion.creationDate.split("T")[0] : ""}</span>
                                </div>
                            </div>
                        )}
                    </div>
                </div>: ""
            }
        </div>

    );
}

export default OtherUser;