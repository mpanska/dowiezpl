import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import React, { useEffect, useRef, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { passwordError, requiredError } from "../utils/validations";
import accountService from "../services/accountService";

function PasswordReset() {
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [confirmError, setConfirmError] = useState(false);
    const [tokenError, setTokenError] = useState(false);

    const history = useHistory();
    const { userId } = useParams();
    const { token } = useParams();
    
    const formRef = useRef();
    const checkButtonRef = useRef();

    useEffect(() => {
        document.body.style.background = "linear-gradient(106.33deg, #FF995F -39.52%, #FFECE1 113.61%)"; 
    }, [])

    const handlePasswordReset = (e) => {
        e.preventDefault();
        formRef.current.validateAll();

        if(newPassword !== confirmPassword){
            setConfirmError(true);
            return;
        }
        
        if(checkButtonRef.current.context._errors.length === 0){
            let userToken = decodeURIComponent(token)
            let data = {
                userId: userId,
                token: userToken,
                password: newPassword
            };

            accountService.resetPassword(data).then(
                response => {
                    console.log(response.data);
                    history.push("/log-in");
                },
                error => {
                    if(error.response.data !== null || error.response.data !== undefined){
                        if(error.response.data.details !== null || error.response.data.details !== undefined){
                            console.log(error.response.data.details[0].code);
                            if(error.response.data.details[0].code === "InvalidToken"){
                                setTokenError(true);
                            }
                        }
                    }
                }
            )
        }
    }

    return ( 
        <div className="login-panel centered-div" style={{padding: "3rem"}}>
            <h2 className="bold">Ustaw nowe hasło</h2>
            <div className="error bold"><p>{tokenError ? "Termin ważności linku użytego do zmiany hasła wygasł" : ""}</p></div>
            <Form ref={formRef} onSubmit={e => handlePasswordReset(e)}>
                <div>
                    <div className="login-panel-input">
                        <label className="login-label" htmlFor="newPassword">Nowe hasło</label>
                        <Input 
                            className={confirmError ? "error-input" : "form-input-width-60"} type="password" id="newPassword" required
                            value={newPassword}
                            onChange={e => setNewPassword(e.target.value)}
                            validations={[requiredError, passwordError]}
                        />
                    </div>
                    <div className="login-panel-input">
                        <label className="login-label" htmlFor="confirmPassword">Potwierdź hasło</label>
                        <Input 
                            className={confirmError ? "error-input" : "form-input-width-60"} type="password" id="confirmPassword" required
                            value={confirmPassword}
                            onChange={e => setConfirmPassword(e.target.value)}
                            validations={[requiredError]}
                        />
                    </div>
                    <div className={confirmError ? "error" : "" }>{confirmError ? "Wprowadzone hasła są różne" : ""}</div>
                </div>
                <button style={{marginTop: "2rem"}} className="login-button basic-button">Zmień hasło</button>
                <CheckButton className="hidden" ref={checkButtonRef}/>
            </Form>
        </div>
    );
}

export default PasswordReset;