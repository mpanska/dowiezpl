import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './scss/index.css';
import LoginPage from "./components/LoginPage";
import Register from "./components/Register";
import MainApp from "./MainApp";
import EmailConfirmed from "./components/elements/EmailConfirmed";
import PasswordReset from "./components/PasswordReset";
import PrivacyPolicy from "./components/PrivacyPolicy";
import Statute from "./components/Statute";
import { UnauthorizedRoute } from "./utils/UnauthorizedRoute";

function App() {
  return (
    <Router>
      <Switch>
        <UnauthorizedRoute path='/log-in' exact component={LoginPage} />
        <UnauthorizedRoute path='/register' exact component={Register} />
        <UnauthorizedRoute path='/confirmEmail/userId/:userId/token/:token' exact component={EmailConfirmed}/>
        <UnauthorizedRoute path='/resetPassword/userId/:userId/token/:token' exact component={PasswordReset}/>
        <Route path='/privacyPolicy' exact component={PrivacyPolicy} />
        <Route path='/statute' exact component={Statute} />
        <Route component={MainApp} />
      </Switch>
    </Router>
  );
}

export default App;
